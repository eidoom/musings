+++
title = "ˈstɔːriz"
description = "DEPRECATED <a href=\"https://eidoom.gitlab.io/stories/\">new site here</a> <br/> A collection of thoughts inspired by media"
template = "index.html"
[extra]
date = 2020-10-01
updated = 2021-06-27
short = "about"
+++

I started this website as a project to maintain discourse in letter form on stories enjoyed during the isolation of coronavirus quarantine.
In more recent times, with the freedom to spontaneously converse in person again, it is coming to more resemble a media log.
I also used it as a pedagogical  exercise in developing a website, although I have yet to correct all the mistakes I've learnt from...

Get started by checking out my [latest posts](@/posts/_index.md).

Warning: [spoilers](https://en.wikipedia.org/wiki/Spoiler_(media)) and [protologisms](https://en.wikipedia.org/wiki/Protologism).

Subscribe to the [RSS/Atom feed](atom.xml), if you like.

Continue the conversation on [Matrix](https://matrix.to/#/@eidoom:matrix.org).
