+++
title = "The Garden of Roses"
description = ""
date = 2021-07-30
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["unreleased"]
artists = ["Philip Pullman"]
series = ["The Book of Dust"]
universe = ["Northern Lights"]
[extra]
#published = 2021-07-30
consumed = []
number = [3]
chrono = 5
links = []
+++
Speculative title.
