+++
title = "To Be Taught, if Fortunate"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = ["science fiction"]
years = ["2019"]
artists = ["Becky Chambers"]
series = []
universe = []
[extra]
active = true
published = 2022-08-23
consumed = [["2022-08-24",""]]
number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/To_Be_Taught,_if_Fortunate"]
+++

