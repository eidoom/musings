+++
title = "The Doors of Stone"
description = ""
date = 2022-08-01
updated = 2022-08-01
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["unreleased"]
artists = ["Patrick Rothfuss"]
series = ["The Kingkiller Chronicle"]
universe = []
[extra]
alt = "The Kingkiller Chronicle: Day Three"
active = false
published = 2022-08-01
consumed = []
number = [3]
#chrono =
links = []
+++

