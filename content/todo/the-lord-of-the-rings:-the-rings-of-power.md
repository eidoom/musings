+++
title = "The Lord of the Rings: The Rings of Power"
description = ""
date = 2022-07-11
updated = 2022-07-11
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = []
years = ["2022"]
artists = []
series = []
universe = ["Middle-Earth"]
[extra]
active = false
published = 2022-07-11
consumed = []
#number = []
chrono = 20.1
links = ["https://en.m.wikipedia.org/wiki/The_Lord_of_the_Rings:_The_Rings_of_Power"]
+++

