+++
title = "The Vela"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2020"]
artists = ["Yoon Ha Lee", "Rivers Solomon", "S.L. Huang", "Becky Chambers"]
series = ["The Vela"]
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
number = [1]
#chrono =
links = []
+++

