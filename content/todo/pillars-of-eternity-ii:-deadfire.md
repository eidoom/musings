+++
title = "Pillars of Eternity II: Deadfire"
description = ""
date = 2022-05-13
updated = 2022-05-13
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2018"]
artists = []
series = ["Pillars of Eternity"]
universe = []
[extra]
active = false
published = 2022-05-13
consumed = []
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Pillars_of_Eternity_II:_Deadfire"]
+++

