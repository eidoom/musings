+++
title = "The Great Gatsby"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1925"]
artists = ["F. Scott Fitzgerald"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = []
#number = 
#chrono =
links = []
+++
