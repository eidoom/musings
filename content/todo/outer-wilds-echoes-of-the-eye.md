+++
title = "Outer Wilds: Echoes of the Eye"
description = ""
date = 2022-06-02
updated = 2022-08-27
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["temporal loop","science fiction","fiction","speculative fiction","aliens","exploration","mystery","story"]
years = ["2021"]
artists = ["Mobius Digital"]
series = ["Outer Wilds"]
universe = []
[extra]
consumed = []
number = [2]
#chrono =
links = []
+++
