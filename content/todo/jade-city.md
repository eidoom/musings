+++
title = "Jade City"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2017"]
artists = ["Fonda Lee"]
series = ["The Green Bone Saga"]
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Jade_City_(novel)"]
+++

