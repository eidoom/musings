+++
title = "Future list"
description = "“The future is still uncertain...”—Future Room, Temple, Black and White."
sort_by = "date"
template = "todo.html"
page_template = "post.html"
paginate_by = 10
insert_anchor_links = "left"
[extra]
short = "Future"
+++
