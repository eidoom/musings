+++
title = "Serpentine"
description = ""
date = 2021-08-09
updated = 2021-08-09
draft = false
[taxonomies]
media = ["book"]
tags = ["novella"]
years = ["2004","2020"]
artists = []
series = ["The Book of Dust"]
universe = ["Northern Lights"]
[extra]
#published = 2021-08-09
consumed = []
number = [0.5]
chrono = 3.6
links = ["https://en.wikipedia.org/wiki/Serpentine_(book)"]
+++

