+++
title = "Edition history"
description = "List of entries ordered by date of latest edit"
template = "edited.html"
[extra]
short = "Edit"
+++
