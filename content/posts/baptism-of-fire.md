+++
title = "Baptism of Fire"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1996","2014"]
artists = ["Andrzej Sapkowski", "David French"]
series = ["The Witcher Saga"]
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
number = [3]
chrono = 5
links = ["https://en.wikipedia.org/wiki/Baptism_of_Fire_(novel)"]
+++
