+++
title = "The Hobbit"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2014"]
artists = []
series = []
universe = ["Middle-Earth"]
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
chrono = 31
links = ["https://en.wikipedia.org/wiki/The_Hobbit_(film_series)"]
+++

