+++
title = "The Last Samurai"
description = "Cue thoughtful flutes"
date = 2021-07-06T22:24:22+01:00
updated = 2021-07-06T22:24:22+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["historical","action","drama","america","united states","japan","epic","war"]
years = ["2003"]
artists = []
series = []
universe = []
[extra]
published = 2021-07-06
consumed = [["2021-07-06"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Last_Samurai"]
+++

