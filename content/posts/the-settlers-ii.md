+++
title = "The Settlers II"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["1996","2006"]
artists = ["Blue Byte"]
series = ["The Settlers"]
universe = []
[extra]
alt = "The Settlers II (10th Anniversary)"
active = false
published = 2022-05-05
consumed = []
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Settlers_II","https://en.wikipedia.org/wiki/The_Settlers_II_(10th_Anniversary)"]
+++

