+++
title = "Arcane"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = []
years = ["2021"]
artists = []
series = []
universe = ["League of Legends"]
[extra]
active = false
published = 2022-04-19
consumed = [["2021-11-29",""]]
#number = []
chrono = 10
links = ["https://en.wikipedia.org/wiki/Arcane_(TV_series)"]
+++

