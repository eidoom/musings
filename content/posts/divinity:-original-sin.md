+++
title = "Divinity: Original Sin"
description = ""
date = 2022-05-13
updated = 2022-05-13
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2014"]
artists = []
series = ["Divinity: Original Sin"]
universe = ["Divinity"]
[extra]
active = true
published = 2022-05-13
consumed = [["2022-05-14",""]]
number = [1]
chrono = 4
links = ["https://en.wikipedia.org/wiki/Divinity:_Original_Sin"]
+++

