+++
title = "The Naked God"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1999"]
artists = ["Peter F. Hamilton"]
series = ["The Night’s Dawn Trilogy"]
universe = ["Confederation universe"]
[extra]
published = 2021-07-30
consumed = []
number = [3]
chrono = 3
links = []
+++
