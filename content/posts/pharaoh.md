+++
title = "Pharaoh"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["1999","2001"]
artists = ["Impressions Games"]
series = []
universe = []
[extra]
alt = "Pharaoh Gold"
active = false
published = 2022-05-05
consumed = []
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Pharaoh_(video_game)"]
+++

Including *Cleopatra: Queen of the Nile* in bundle *Pharaoh Gold*.
