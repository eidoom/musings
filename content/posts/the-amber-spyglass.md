+++
title = "The Amber Spyglass"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2000"]
artists = ["Philip Pullman"]
series = ["His Dark Materials"]
universe = ["Northern Lights"]
[extra]
published = 2021-07-30
consumed = []
number = [3]
chrono = 3
links = []
+++
