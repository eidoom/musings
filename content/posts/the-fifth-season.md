+++
title = "The Fifth Season"
description = ""
date = 2020-07-08
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2015"]
artists = ["N.K. Jemisin"]
series = ["The Broken Earth"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-07-15", "2020-07-18"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Fifth_Season_(novel)"]
+++
