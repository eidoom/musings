+++
title = "Fun Friday Facts"
description = "The Factorio blog"
date = 2021-02-08T23:08:39+00:00
updated = 2021-02-08T23:08:39+00:00
draft = false
[taxonomies]
media = ["web"]
formats = ["blog"]
tags = []
years = ["2012"]
artists = ["Wube Software"]
series = []
universe = []
[extra]
consumed = []
#number = 
#chrono =
links = ["https://www.factorio.com/blog/"]
+++

I enjoy following the [Factorio](@/posts/factorio.md) development blog.
It’s fun to see the solutions they come up with for problems and hear their thoughts on designing the game.
