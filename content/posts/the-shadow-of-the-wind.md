+++
title = "The Shadow of the Wind"
description = ""
date = 2019-07-01
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2001"]
artists = ["Carlos Ruiz Zafón", "Lucia Graves"]
series = ["The Cemetery of Forgotten Books"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [1]
#chrono =
links = []
+++
