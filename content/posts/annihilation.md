+++
title = "Annihilation"
description = ""
date = 2019-12-27
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2018"]
artists = ["Catherynne M. Valente"]
series = ["Mass Effect: Andromeda"]
universe = ["Mass Effect"]
[extra]
published = 2021-07-30
consumed = [["2020-06-09", "2020-06-14"]]
number = [3]
chrono = 3.5
links = ["https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda_(book_series)#Annihilation"]
+++
