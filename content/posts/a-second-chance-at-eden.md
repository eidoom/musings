+++
title = "A Second Chance at Eden"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["short story","collection"]
tags = ["science fiction"]
years = ["1998"]
artists = ["Peter F. Hamilton"]
series = []
universe = ["Confederation universe"]
[extra]
published = 2021-07-30
consumed = []
#number = 
chrono = 0
links = ["https://en.wikipedia.org/wiki/A_Second_Chance_at_Eden"]
+++
