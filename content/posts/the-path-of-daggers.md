+++
title = "The Path of Daggers"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1998"]
artists = ["Robert Jordan"]
series = ["The Wheel of Time"]
universe = ["The Wheel of Time"]
[extra]
published = 2021-07-30
consumed = []
number = [8]
chrono = 8
links = []
+++
