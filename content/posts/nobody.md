+++
title = "Nobody"
description = "John Wick but with a kitty bracelet"
date = 2021-07-05T08:20:04+01:00
updated = 2021-07-05T08:20:04+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["action","thriller"]
years = ["2021"]
artists = []
series = []
universe = []
[extra]
published = 2021-07-05
consumed = [["2021-07-04"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Nobody_(2021_film)"]
+++

