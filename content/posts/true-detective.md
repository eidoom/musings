+++
title = "True Detective"
description = "Superbly dark neo-noir"
date = 2021-04-22T08:12:56+01:00
updated = 2021-10-23
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["crime","drama","fiction","anthology","mystery","detective","neo-noir","nonchronological","nonlinear"]
years = ["2014"]
artists = []
series = []
universe = []
[extra]
consumed = [["2020-06-06",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/True_Detective"]
+++

Great title sequences.

Society is *messed up*.
The immoral precipitate out at the top.
Policing exacts the highest toll on its officers.

My only complaint is that it is extremely difficult for me to understand what the detectives are actually saying in their aesthetic gravelly accents.

{{ season(number=1, link="https://en.wikipedia.org/wiki/True_Detective_(season_1)", year=2014) }}

Outstanding.

{{ season(number=2, started="2021-04-19", finished="2021-04-21", link="https://en.wikipedia.org/wiki/True_Detective_(season_2)", year=2015) }}

A tale of the sordid corruption amongst leaders, exploitation of the poor, vigilante justice, an honourable gangster, and the soul-destroying burden of serving in the police.
We follow three cops: Velcoro, whose bosses are possibly less lawful than his unofficial boss, mobster Semyon; Bezzerides, damaged by a hippy commune upbringing and subsequent falling apart of her family; and Woodrugh, a young closeted veteran of a traumatic war.
It felt like we were only scraping the surface of the characters’ lives, giving the sense of a deep tapestry beneath the short story.

{{ season(number=3, started="2021-10-16", finished="2021-10-19", link="https://en.wikipedia.org/wiki/True_Detective_(season_3)", year=2019) }}

The third season returns to the tried-and-tested recipe of the first, but subverts some of the assumptions that come with it.

Our police partners, Wayne Hays and Roland West, have a close but tumultuous bond, Wayne often pushing the limits of their relationship.
They manage to come through in the end with an intact friendship, though.

The reveal of Will Purcell’s murder as accidental and Julie Purcell’s kidnapper as Isabel Hoyt, a mother driven to madness by the grief of losing her daughter in a car crash, is a sad and moving counterpoint to Elisa Montogomery’s season one-esque theories.

Harris James is the closest thing we have to a traditional antagonist, the psychopathic murderer, but what is his motive? Money? Loyalty to the Hoyts? Schadenfreude? He croaks before we can find out.

The racism that Native American Brett Woodard suffers is tragic.
His last stand is the season’s action high.

Amelia Reardon is keenly intelligent and empathetic.
She’s a better investigator than our cop duo.
As “his woman”, however, Wayne jealously disapproves of and disregards her sleuthing.
Wayne’s often awful treatment of Amelia, withholding the trust she deserves and taking out his frustration with his work on her, is the unforgiveable nadir of his character.
I hope he was a better husband after quitting the force in 1990.
He does at least come to understand and regret this in his last days, after she is gone.

Lucy Purcell is completely unlikeable.
It’s no surprise that Amelia’s suspicions were correct in that she was involved in the kidnapping.
Tom Purcell does an excellent job of being incredibly annoying.
The friendship that slowly precipitates between him and Roland is touching.

Behind Roland’s laddish exterior, his deep compassion is often revealed through moments like his caring of Tom.
He keeps Wayne under his wing from day one too.
His best friend is a dog.

Interwoven through the whole plot, Wayne’s struggle to live with dementia and recall his life is bittersweet.
An elderly and retired man in 2015, he is still a very capable detective, perhaps more so than in his youth because of his acceptance of Amelia’s work.
The conveyance of his state of mind, in particular when his mind transitions into forgetfulness or haunting, through sound and the expression on his face, is chilling and sympathetically emotional.
