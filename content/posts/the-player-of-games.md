+++
title = "The Player of Games"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1988"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 4
links = ["https://en.wikipedia.org/wiki/The_Player_of_Games"]
+++
