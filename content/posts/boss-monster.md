+++
title = "Boss Monster: The Dungeon Building Card Game "
description = ""
date = 2022-08-16
updated = 2022-08-16
draft = false
[taxonomies]
media = ["game"]
formats = ["card game"]
tags = ["tabletop"]
years = ["2013"]
artists = []
series = []
universe = []
[extra]
alt = "Boss Monster 2: The Next Level"
active = false
published = 2022-08-16
consumed = [["2022-08-15"]]
#number = []
#chrono =
links = ["https://boardgamegeek.com/boardgame/131835/boss-monster-dungeon-building-card-game"]
+++

Played with 4 players.
