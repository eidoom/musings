+++
title = "Ori and the Blind Forest"
description = "A masterpiece of art"
date = 2020-11-06T10:28:15+00:00
updated = 2020-11-06T10:28:15+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["platformer","adventure","metroidvania","2.5d","fantasy", "speculative fiction","fiction"]
years = ["2015"]
artists = ["Moon Studios"]
series = ["Ori"]
universe = []
[extra]
completed = false
consumed = [["2015-11-03", ""]] 
number = [1]
links = ["https://en.wikipedia.org/wiki/Ori_and_the_Blind_Forest","https://www.orithegame.com/blind-forest/"]
+++

I’m not much of a player of platformers, but this game deserved my attention.
The visual artwork, story, and music come together to create a truly beautiful game.
