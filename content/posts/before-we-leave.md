+++
title = "Before We Leave"
description = "Peaceful and pretty civilisation rebuilder"
date = 2021-08-13
updated = 2021-08-13
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["city builder"]
years = ["2020"]
artists = ["Balancing Monkey Games"]
series = []
universe = []
[extra]
published = 2021-08-13
consumed = [["2021-08-13",""]]
#number = []
#chrono =
links = ["https://www.balancingmonkeygames.com/before-we-leave"]
+++

