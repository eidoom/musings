+++
title = "The Matrix Resurrections"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2021"]
artists = []
series = ["The Matrix"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-01-08"]]
number = [4]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Matrix_Resurrections"]
+++

