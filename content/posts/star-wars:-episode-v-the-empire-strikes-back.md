+++
title = "Star Wars: Episode V – The Empire Strikes Back"
description = ""
date = 2022-05-21
updated = 2022-05-21
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1980"]
artists = []
series = ["Skywalker saga","Star Wars original trilogy"]
universe = ["Star Wars"]
[extra]
active = false
published = 2022-05-21
consumed = []
number = [5,2]
chrono = 5
links = ["https://en.wikipedia.org/wiki/The_Empire_Strikes_Back"]
+++

