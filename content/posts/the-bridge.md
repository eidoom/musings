+++
title = "The Bridge"
description = "The Fifth Bridge? The Forth to Fife?"
date = 2021-01-17T14:37:14+00:00
updated = 2021-08-19
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fiction","scotland","drama","dream","united kingdom","romance","amnesia","literary","britain"]
years = ["1986"]
artists = ["Iain Banks"]
series = []
universe = []
[extra]
consumed = [["2021-01-17","2021-02-04"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Bridge_(novel)"]
+++

This is the story of John Orr, not that it’s his real name.
After a car crash, he falls into a coma in which he lives in a surreal dreamscape.
We jump between his personae: John on the Bridge, the barbarian scouring [a Culturesque universe](../../universe/the-culture), and memories of his life up to the crash.

Like [*Walking on Glass*](@/posts/walking-on-glass.md) before it, the chapters are cleverly named.
We start with *Coma*, clearly setting the scene, and end, appropriately, on the alliterative and rhyming *Coda*.
Some take their titles from geological {{ wikipedia(name="periods", slug="Geological_period") }} and {{ wikipedia(name="epochs", slug="Epoch_(geology)") }}: *{{ wikipedia(name="Triassic") }}*, *{{ wikipedia(name="Eocene") }}*, *{{ wikipedia(name="Oligocene") }}*, *{{ wikipedia(name="Miocene") }}*, *{{ wikipedia(name="Pliocene") }}*, and *{{ wikipedia(name="Quaternary") }}*; the others are an indexed *{{ wikipedia(name="Metamorphosis", slug="Metamorphism") }}*.
The allusion to geology is unsurprising considering the novel’s strong geographical rooting and {{ wikipedia(name="Scotland’s rich geology", slug="Geology_of_Scotland") }}.
The countdown through eras mirrors our approach to the present day in John’s dream trance reliving of his life and the metaphorism the changes within the splinters of his personality.

The Bridge is a construction of seemingly endless span with a city unraveled and spread along it.
It is a reflection of the {{ wikipedia(name="Forth Bridge") }}, a recurring landmark in John’s real life.
Directions are denoted Cityward and Kingdomward: echoes of the City of {{ wikipedia(name="Edinburgh") }} and the Kingdom of {{ wikipedia(name="Fife") }}.
The imagery of the Bridge fractally appears at every level of the dream, for example when John likens the pattern of Abberlaine Arrol’s stockings to the crossed beams of the Bridge[^arrol].
Life on the bridge is totalitarian and dystopian, an indication of socialist John’s political discontent in Tory Britian through the Eighties.

As in the later *Feersum Endjinn*, the barbarian sequences are entertainingly depicted in phonetic Scots.
Here lies John’s base emotions, his suppressed aggression, the feelings he is most shameful of.
He has a knockoff Knife Missile—perhaps he’s a [*Culture*](../../universe/the-culture) reader?

John’s real life is full of internal conflict and contradiction, a struggle for ownership of an identity; it’s no surprise he self-destructively ends up in a coma that he prefers to linger in, amnesiac, forgetting even his own name.
He grows up in working-class Glasgow, but flies the nest for university and to lead an engineering business on the East Coast.
The most important part of his life is Andrea Cramond (Abberlaine’s doppelgänger), with whom he is in an open relationship, a situation he both loves and tortures himself over.

[^arrol]: Civil engineer {{ wikipedia(name="William Arrol") }} was one of the Forth Bridge builders.
