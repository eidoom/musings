+++
title = "Star Wars: Episode I – The Phantom Menace"
description = ""
date = 2022-05-21
updated = 2022-05-21
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1999"]
artists = []
series = ["Skywalker saga","Star Wars prequel trilogy"]
universe = ["Star Wars"]
[extra]
active = false
published = 2022-05-21
consumed = []
number = [1,1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Star_Wars:_Episode_I_%E2%80%93_The_Phantom_Menace"]
+++

