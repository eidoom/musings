+++
title = "A Few Notes on the Culture"
description = "Essential reading for Culture aficionados"
date = 2021-01-27T10:13:43+00:00
updated = 2021-02-07
draft = false
[taxonomies]
media = ["web"]
formats = ["essay"]
tags = []
years = ["1994"]
artists = ["Iain M. Banks"]
series = []
universe = ["The Culture"]
[extra]
consumed = [["2021-01-27"]]
#number = 
chrono = 0
links = ["http://www.vavatch.co.uk/books/banks/cultnote.htm"]
+++
