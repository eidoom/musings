+++
title = "The Wandering Earth"
description = ""
date = 2021-10-03
updated = 2021-10-03
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["fiction","science fiction","speculative fiction","disaster","action","thriller"]
years = ["2019"]
artists = []
series = []
universe = []
[extra]
language = "Mandarin"
alt = "流浪地球"
active = false
published = 2021-10-03
consumed = [["2021-10-02"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Wandering_Earth"]
+++

