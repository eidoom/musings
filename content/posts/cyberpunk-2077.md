+++
title = "Cyberpunk 2077"
description = "Down with the corpo scum!"
date = 2021-02-11T11:12:22+00:00
updated = 2021-05-03
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["cyberpunk","corporate capitalism","rpg","adventure","action","fiction","speculative fiction","science fiction","dystopian"]
years = ["2020"]
artists = ["CD Projekt Red"]
series = []
universe = []
[extra]
completed = true
consumed = [["2021-02-10",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Cyberpunk_2077","https://www.cyberpunk.net"]
+++

{{ playthrough(name="Streetkid playthough",started="2021-02-10",finished="") }}
I enjoyed this game.
