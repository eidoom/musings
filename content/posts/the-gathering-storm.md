+++
title = "The Gathering Storm"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2009"]
artists = ["Robert Jordan", "Brandon Sanderson"]
series = ["The Wheel of Time"]
universe = ["The Wheel of Time"]
[extra]
published = 2021-07-30
consumed = []
number = [12]
chrono = 12
links = []
+++
