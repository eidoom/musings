+++
title = "The Telling"
description = "A story about stories"
date = 2021-07-10T13:09:20+01:00
updated = 2021-07-29
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction","science fiction","fiction","far future"]
years = ["2000"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
published = 2021-07-29
consumed = [["2021-07-25","2021-07-29"]]
number = [8]
chrono = 8
links = ["https://en.wikipedia.org/wiki/The_Telling"]
+++

> “...belief is the wound that knowledge heals.”

Sutty, who lived on Terra through the violent religious oppression of the Unists and lost her partner Poe to that brutality, becomes an Ekumenical Observer on the planet Aka.
There, the ancient traditions of storytelling which formed the soul of a global society have been suppressed by a new corporate state born of the suffering created by a minority group of knowledge-usurers and exposure of the isolated civilisation to the Ekumen.

The depiction of the political climate of Aka reminds me of Liu Cixin’s description of the Cultural Revolution in China in [*The Three-Body Problem*](@/posts/the-three-body-problem.md).
Le Guin states in the preface that the parallels to this part of Chinese history are deliberate.
