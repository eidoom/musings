+++
title = "How Long ’til Black Future Month"
description = "The sketchpad of an incredible author"
date = 2020-10-12T21:22:07+01:00
updated = 2020-10-12T21:22:07+01:00
[taxonomies]
media = ["book"]
formats = ["collection","short story"]
tags = ["fantasy", "speculative fiction","fiction"]
years = ["2018"]
artists = ["N. K. Jemisin"]
series = []
universe = []
[extra]
consumed = [["2020-09-09", "2020-09-21"]]
#number = 
links = ["https://en.wikipedia.org/wiki/How_Long_%27til_Black_Future_Month%3F"]
+++

A great collection of fantasy and sci-fi short stories.
They are diverse and test out wonderfully imagined, ambitious ideas.
