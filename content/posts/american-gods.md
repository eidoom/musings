+++
title = "American Gods"
description = "Specifically, the New versus the Old"
date = 2021-01-10T22:00:31+00:00
updated = 2021-03-22
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["drama","fantasy", "speculative fiction","united states","comedy","fiction","america"]
years = ["2017"]
artists = []
series = []
universe = []
[extra]
consumed = [["2018-10-01",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/American_Gods_(TV_series)"]
+++

{{ season(number=3, started="2021-02-01", finished="2021-03-22", year=2021, link="https://en.wikipedia.org/wiki/American_Gods_(season_3)") }}
