+++
title = "Kingdom Come: Deliverance"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2018"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2018-11-17",""]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Kingdom_Come:_Deliverance"]
+++

Including *The Amorous Adventures of Bold Sir Hans Capon*.
