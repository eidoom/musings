+++
title = "Noughts + Crosses"
description = ""
date = 2020-12-05T14:20:41+00:00
updated = 2020-12-05T14:20:41+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["drama","england","alternative history","romance","bildungsroman","tragedy","racism","prejudice","discrimination"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
published = 2021-09-05
consumed = [["2020-03-06", ""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Noughts_%2B_Crosses"]
+++

{{ season(number=1,started="2020-03-06", finished="2020-04-10") }}
