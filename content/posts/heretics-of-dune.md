+++
title = "Heretics of Dune"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1984"]
artists = ["Frank Herbert"]
series = ["Dune Chronicles"]
universe = ["Dune"]
[extra]
published = 2021-07-30
consumed = []
number = [5]
chrono = 5
links = ["https://en.wikipedia.org/wiki/Heretics_of_Dune"]
+++
