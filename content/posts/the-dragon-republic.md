+++
title = "The Dragon Republic"
description = ""
date = 2022-06-28
updated = 2022-06-28
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["grimdark","epic fantasy","fantasy","high fantasy"]
years = ["2019"]
artists = ["R. F. Kuang"]
series = ["The Poppy War Trilogy"]
universe = []
[extra]
active = false
published = 2022-06-28
consumed = [["2022-06-28","2022-07-10"]]
number = [2]
#chrono =
links = []
+++

