+++
title = "Crossroads of Twilight"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2003"]
artists = ["Robert Jordan"]
series = ["The Wheel of Time"]
universe = ["The Wheel of Time"]
[extra]
published = 2021-07-30
consumed = []
number = [10]
chrono = 10
links = []
+++
