+++
title = "Supernova Era"
description = "Children inherit the Earth"
date = 2020-11-29T13:11:20+00:00
updated = 2021-02-20
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction","speculative fiction","fiction","hard sci-fi","china"]
years = ["2003","2019"]
artists = ["Joel Martinsen","Liu Cixin"]
series = []
universe = []
[extra]
alt="超新星纪元"
consumed = [["2020-11-29", "2020-12-12"]] 
#number = 
links = ["https://en.wikipedia.org/wiki/Liu_Cixin#Novels"]
+++

*Supernova Era* (超新星纪元) marks somewhat of a departure from Liu Cixin’s other novels.
The story goes from strange to absurd as genetically resilient children take over the world after all adults die due to radiation from a nearby supernova.
Their actions reflect modern society’s floundering in the face of new technologies as demonstrated by global social division and the continuing ecological damage of the last century.
We end on an uplifting note, however, as thirty years into the Supernova Era we see people watching the Earth rise over the Martian horizon.
