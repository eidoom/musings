+++
title = "Unfinished Tales"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["collection"]
tags = ["fantasy"]
years = ["1980"]
artists = ["J. R.R. Tolkien", "Christopher Tolkien"]
series = ["Tolkien’s legendarium"]
universe = ["Middle-Earth"]
[extra]
alt = "Unfinished Tales of Númenor and Middle-Earth"
published = 2021-07-30
consumed = []
number = [2]
chrono = 21
links = ["https://en.wikipedia.org/wiki/Unfinished_Tales"]
+++
