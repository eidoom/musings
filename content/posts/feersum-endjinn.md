+++
title = "Feersum Endjinn"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["1994"]
artists = ["Iain M. Banks"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-01-23", "2020-02-18"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Feersum_Endjinn"]
+++
