+++
title = "Central Station"
description = ""
date = 2021-09-19
updated = 2021-09-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["drama"]
years = ["1998"]
artists = []
series = []
universe = []
[extra]
language="Portuguese"
alt="Central do Brasil"
active = false
published = 2021-09-19
consumed = [["2021-09-16"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Central_Station_%28film%29"]
+++

