+++
title = "Chapterhouse: Dune"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1985"]
artists = ["Frank Herbert"]
series = ["Dune Chronicles"]
universe = ["Dune"]
[extra]
published = 2021-07-30
consumed = []
number = [6]
chrono = 6
links = ["https://en.wikipedia.org/wiki/Chapterhouse:_Dune"]
+++
