+++
title = "Fahrenheit 451"
description = ""
date = 2019-12-02
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["dystopian","science fiction"]
years = ["1953"]
artists = ["Ray Bradbury"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-03-31", "2020-04-06"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Fahrenheit_451"]
+++
