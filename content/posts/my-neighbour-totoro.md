+++
title = "My Neighbour Totoro"
description = "Delightful, wholesome"
date = 2021-08-10
updated = 2021-08-10
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["anime","japan","fantasy","animated","family friendly"]
years = ["1988","2006"]
artists = ["Studio Ghibli"]
series = []
universe = []
[extra]
alt = "となりのトトロ"
published = 2021-08-10
consumed = [["2021-08-10"]]
#number = []
#chrono =
links = []
+++

