+++
title = "The Shadowed Sun"
description = ""
date = 2020-08-20
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2012"]
artists = ["N.K. Jemisin"]
series = ["Dreamblood"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-08-29", "2020-09-09"]]
number = [2]
#chrono =
links = []
+++
