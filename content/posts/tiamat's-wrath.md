+++
title = "Tiamat’s Wrath"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2019"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [8]
chrono = 8
links = ["https://en.wikipedia.org/wiki/Tiamat%27s_Wrath"]
+++
