+++
title = "Fargo"
description = "Dark humour at its finest"
date = 2021-01-06T12:43:47+00:00
updated = 2021-02-07
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["black comedy","crime","anthology","thriller","drama"]
years = ["2014"]
artists = []
series = []
universe = []
[extra]
consumed = [["2017-10-01",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Fargo_%28TV_series%29"]
+++

{{ season(number=4, started="2021-02-02", finished="2021-02-05", year=2020, link="https://en.wikipedia.org/wiki/Fargo_(season_4)") }}

In the 1950s, the newly arrived Cannons, a Black crime family, come to blows with the established Fadda Italian mafia.

A strong season of Fargo.
