+++
title = "Great North Road"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2012"]
artists = ["Peter F. Hamilton"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = []
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Great_North_Road_(novel)"]
+++
