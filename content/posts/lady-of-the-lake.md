+++
title = "The Lady of the Lake"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1996","2017"]
artists = ["Andrzej Sapkowski", "David French"]
series = ["The Witcher Saga"]
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
number = [5]
chrono = 7
links = ["https://en.wikipedia.org/wiki/The_Lady_of_the_Lake_(Sapkowski_novel)"]
+++
