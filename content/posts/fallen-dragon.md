+++
title = "Fallen Dragon"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2001"]
artists = ["Peter F. Hamilton"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = []
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Fallen_Dragon"]
+++
