+++
title = "La Belle Sauvage"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2017"]
artists = ["Philip Pullman"]
series = ["The Book of Dust"]
universe = ["Northern Lights"]
[extra]
published = 2021-07-30
consumed = [["2019-11-25", "2019-12-06"]]
number = [1]
chrono = 0
links = ["https://en.wikipedia.org/wiki/La_Belle_Sauvage"]
+++
