+++
title = "A Wizard of Earthsea"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1968"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = ["Earthsea"]
[extra]
active = false
published = 2022-04-19
consumed = [["2021-11-20","2021-12-23"]]
number = [1]
chrono = 3
links = ["https://en.wikipedia.org/wiki/A_Wizard_of_Earthsea"]
+++

