+++
title = "Elite Dangerous"
description = "Space is big and empty"
date = 2020-11-22
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["space","simulator","science fiction","speculative fiction","space flight sim","trading","combat"]
years = ["2014"]
artists = ["Frontier Developments"]
series = []
universe = []
[extra]
consumed = [["2015-07-12", ""]] 
#number = 
links = ["https://en.wikipedia.org/wiki/Elite_Dangerous","https://www.elitedangerous.com/"]
+++

Including *Elite Dangerous: Horizons* expansion.

The world of this game encompasses a simulation of the entire Milky Way.
Travelling around, you get an agoraphobic sense of the scale of the galaxy and have some seriously megalophobic encounters with stellar bodies.
