+++
title = "Northern Lights"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1995"]
artists = ["Philip Pullman"]
series = ["His Dark Materials"]
universe = ["Northern Lights"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Northern_Lights_(Pullman_novel)"]
+++
