+++
title = "The Night Cage"
description = ""
date = 2022-04-30
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = []
years = ["2021"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-04-30
consumed = [["2022-04-29"],["2022-08-10"]]
#number = []
#chrono =
links = ["https://www.thenightcage.com/","https://boardgamegeek.com/boardgame/306709/night-cage"]
+++

I played with five players.
