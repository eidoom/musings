+++
title = "Look to Windward"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2000"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = [["2019-06-05","2019-06-29"]]
number = [7]
chrono = 7
links = ["https://en.wikipedia.org/wiki/Look_to_Windward"]
+++
