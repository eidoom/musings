+++
title = "Life is Strange"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["comic"]
tags = []
years = ["2018"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
#chrono =
links = ["https://life-is-strange.fandom.com/wiki/Life_is_Strange_(Comic_Series)"]
+++

Read:
Life Is Strange: Dust, Life Is Strange: Waves, Life Is Strange: Strings.

Unread:
Life Is Strange: Partners in Time: Tracks, Life is Strange: Coming Home, Life is Strange: Settling Dust.
