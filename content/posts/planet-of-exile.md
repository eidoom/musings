+++
title = "Planet of Exile"
description = "Going native"
date = 2021-06-02T12:52:26+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction","science fiction","fiction","far future","fantasy"]
years = ["1966"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
published = 2021-06-08
consumed = [["2021-06-02","2021-06-05"]]
number = [2]
chrono = 4
links = ["https://en.wikipedia.org/wiki/Planet_of_Exile"]
+++

An intriguing primitive humanoid society bound by their climate, sharing curious interactions with stranded interlopers.

In the cycle, we establish Werel and that the Enemy has engaged in the War.
