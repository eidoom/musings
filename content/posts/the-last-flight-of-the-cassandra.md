+++
title = "The Last Flight of the Cassandra"
description = "It’ll definitely be fine to land on this asteroid"
date = 2021-07-30
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["short story"]
tags = []
years = ["2019"]
artists = []
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [1.1]
chrono = 1.1
links = []
+++

