+++
title = "Stranger Things"
description = ""
date = 2022-07-19
updated = 2022-07-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["horror","drama","science fiction","speculative fiction"]
years = ["2016"]
artists = []
series = []
universe = []
[extra]
active = true
published = 2022-07-19
consumed = []
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Stranger_Things"]
+++

{{ season(number=1, started="2022-07-16", finished="2022-07-19", link="https://en.wikipedia.org/wiki/Stranger_Things_(season_1)", year=2016) }}

{{ season(number=2, started="2022-07-24", finished="2022-07-29", link="https://en.wikipedia.org/wiki/Stranger_Things_(season_2)", year=2017) }}

{{ season(number=3, started="2022-07-26", finished="2022-08-27", link="https://en.wikipedia.org/wiki/Stranger_Things_(season_3)", year=2019) }}

{{ season(number=4, started="2022-08-27", link="https://en.wikipedia.org/wiki/Stranger_Things_(season_4)", year=2022) }}
