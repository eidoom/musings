+++
title = "Fire & Blood"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2018"]
artists = ["George R. R. Martin"]
series = ["A Targaryen History"]
universe = ["Game of Thrones"]
[extra]
published = 2021-07-30
consumed = [["2020-06-14", "2020-07-15"]]
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Fire_%26_Blood_(novel)"]
+++
