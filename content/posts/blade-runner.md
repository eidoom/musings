+++
title = "Blade Runner"
description = ""
date = 2021-10-25
updated = 2021-10-25
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1982"]
artists = []
series = ["Blade Runner"]
universe = []
[extra]
active = false
published = 2021-10-25
consumed = []
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Blade_Runner"]
+++

