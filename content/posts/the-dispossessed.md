+++
title = "The Dispossessed"
description = "Powerful politically, socially"
date = 2021-06-23T11:00:44+01:00
updated = 2021-08-07
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction","science fiction","fiction","far future","utopian","solarpunk"]
years = ["1974"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
alt = "The Dispossessed: An Ambiguous Utopia"
published = 2021-07-01
consumed = [["2021-06-24","2021-06-30"]]
number = [6]
chrono = 1
links = ["https://en.wikipedia.org/wiki/The_Dispossessed"]
+++

Wow.
These stories just keep getting better.
Very thought provoking on the structure of power within society.

The inhabitants of Anarres and Urras, a binary planet system of the star Tau Ceti, are the Cetians.
The superpowers on the primary planet, Anarres, are A-Io, a familiar capitalist society, and Thu, a authoritarian communist state.
The moon, Urras, is inhabited by the anarcho-syndicalist Odonians—our frontier outpost utopia.
The protagonist, Shevek, conceives of the theory that leads to the ansible (and maybe [later](@/posts/rocannon's-world.md) to FTL travel, although Le Guin herself [reminds](@/posts/the-birthday-of-the-world-and-other-stories.md#foreword) us that consistency was never the purpose of the so-called Hainish Cycle).
