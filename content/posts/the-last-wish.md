+++
title = "The Last Wish"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["short story","collection"]
tags = []
years = ["1993","2007"]
artists = ["Andrzej Sapkowski", "Danusia Stok"]
series = []
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
#number = []
chrono = 1
links = ["https://en.wikipedia.org/wiki/The_Last_Wish_(book)"]
+++
