+++
title = "Lyra’s Oxford"
description = ""
date = 2019-12-27
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2003"]
artists = ["Philip Pullman"]
series = ["His Dark Materials"]
universe = ["Northern Lights"]
[extra]
published = 2021-07-30
consumed = [["2019-12-27", "2019-12-28"]]
number = [3.5]
chrono = 3.4
links = ["https://en.wikipedia.org/wiki/Lyra%27s_Oxford"]
+++
