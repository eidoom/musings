+++
title = "Lego The Lord of the Rings"
description = ""
date = 2022-04-21
updated = 2022-04-21
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2013"]
artists = []
series = []
universe = ["Middle-Earth"]
[extra]
active = false
published = 2022-04-21
consumed = [["2022-04-20",""]]
#number = []
chrono = 31.4
links = ["https://en.wikipedia.org/wiki/Lego_The_Lord_of_the_Rings_(video_game)"]
+++

