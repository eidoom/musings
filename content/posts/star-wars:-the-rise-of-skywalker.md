+++
title = "Star Wars: Episode IX – The Rise of Skywalker"
description = "It’s always Palpatine"
date = 2020-11-29T16:57:10+00:00
updated = 2021-02-17
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["epic","science fiction","fantasy", "speculative fiction","space opera","space","fiction"]
years = ["2019"]
artists = []
series = ["Skywalker saga","Star Wars sequel trilogy"]
universe = ["Star Wars"]
[extra]
consumed = [["2020-11-29"]]
number = [9,3]
chrono = 9
links = ["https://en.wikipedia.org/wiki/Star_Wars:_The_Rise_of_Skywalker"]
+++

