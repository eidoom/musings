+++
title = "The Orville"
description = ""
date = 2022-06-15
updated = 2022-06-15
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = []
years = ["2017"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-06-15
consumed = []
#number = []
#chrono =
links = ["https://en.m.wikipedia.org/wiki/The_Orville"]
+++

{{ season(number=3, started="2022-06-13", finished="2022-08-24", year=2022, link="https://en.m.wikipedia.org/wiki/The_Orville_(season_3)") }}
