+++
title = "Always Coming Home"
description = "So you are here"
date = 2021-08-09
updated = 2021-11-17
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fiction","speculative fiction","science fiction","anthropological","ethnographical","pseudo-textbook"]
years = ["1985"]
artists = ["Ursula K. Le Guin"]
series = []
universe = []
[extra]
alt = "Always Coming Home: Author’s Expanded Edition"
active = false
published = 2021-11-17
consumed = [["2021-08-27","2021-11-17"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Always_Coming_Home"]
+++

This is a novel truly like no other I have ever read.

In reading, I recall the little green men of [The Word for World Is Forest](../the-word-for-world-is-forest) and the people of [Shantih Town](../the-eye-of-the-heron).
The setting echoes of [Eleven-Soro](../../posts/the-birthday-of-the-world-and-other-stories/#solitude), a short story yet to be written in the time of this novel, with a “primitive” society living in the ashes of the post-apocalyse. 
The moieties remind me of the planet O, also to be [later](../../posts/the-birthday-of-the-world-and-other-stories/#unchosen-love) [explored](../../posts/the-birthday-of-the-world-and-other-stories/#mountain-ways).
The oral heritage is returned to in [The Telling](../the-telling).
This work is pure Le Guin.

The people are of course a homage to Native Americans.
Taoism is ever present, heya hey.

The City of the Mind, a network of independent artificial intelligence interwoven through the solar system, is a tantalising and fascinating thought of science fiction.

The society explored in this book is different, wonderful, challenging.
It makes one question the definition of civilisational “progress”.
It's difficult to describe in justice with any less words than those of the entire fiction.
Ask me about it for a story tale.
