+++
title = "The Green Planet"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television","documentary"]
tags = []
years = ["2022"]
artists = ["David Attenborough"]
series = []
universe = []
[extra]
active = false
#published = 2022-04-19
consumed = [["2022-02-07","2022-03-02"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Green_Planet_(TV_series)"]
+++

