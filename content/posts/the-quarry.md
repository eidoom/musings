+++
title = "The Quarry"
description = ""
date = 2022-08-20
updated = 2022-08-20
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["horror","interactive drama","survival","survival horror"]
years = ["2022"]
artists = ["Supermassive Games"]
series = []
universe = []
[extra]
active = false
published = 2022-08-20
consumed = [["2022-08-19","2022-08-20"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Quarry_(video_game)"]
+++

Played with 9 people.
