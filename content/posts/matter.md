+++
title = "Matter"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2008"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = [["2019-06-29", "2019-07-22"]]
number = [8]
chrono = 6
links = ["https://en.wikipedia.org/wiki/Matter_(novel)"]
+++
