+++
title = "Red Seas Under Red Skies"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2007"]
artists = ["Scott Lynch"]
series = ["Gentleman Bastard"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [2]
#chrono =
links = []
+++
