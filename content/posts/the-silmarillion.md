+++
title = "The Silmarillion"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["collection"]
tags = ["mythopoeia"]
years = ["1977"]
artists = ["J. R. R. Tolkien", "Christopher Tolkien"]
series = ["Tolkien’s legendarium"]
universe = ["Middle-Earth"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 20
links = ["https://en.wikipedia.org/wiki/The_Silmarillion"]
+++
