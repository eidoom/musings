+++
title = "Eternal Sunshine of the Spotless Mind"
description = "To forget"
date = 2021-05-21T08:34:41+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["speculative fiction","drama","comedy","science fiction","romance","fiction"]
years = ["2004"]
artists = []
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-05-20"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Eternal_Sunshine_of_the_Spotless_Mind"]
+++

Quirky, charming, funny.
