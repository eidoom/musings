+++
title = "Broken Angels"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2003"]
artists = ["Richard K. Morgan"]
series = ["Takeshi Kovacs"]
universe = ["Altered Carbon"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/Broken_Angels_(novel)"]
+++
