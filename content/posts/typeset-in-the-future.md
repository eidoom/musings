+++
title = "Typeset In The Future"
description = "Nerd humour and the stories in the fonts of sci-fi films"
date = 2021-08-12
updated = 2021-08-12
draft = false
[taxonomies]
media = ["web"]
formats = ["blog"]
tags = []
years = ["2014"]
artists = []
series = []
universe = []
[extra]
#published = 2021-08-12
consumed = []
#number = []
#chrono =
links = ["https://typesetinthefuture.com/"]
+++

