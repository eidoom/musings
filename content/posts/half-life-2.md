+++
title = "Half-Life 2"
description = ""
date = 2022-04-21
updated = 2022-04-21
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2004"]
artists = []
series = ["Half-Life"]
universe = ["Half-Life"]
[extra]
active = false
published = 2022-04-21
consumed = []
number = [2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/Half-Life_2"]
+++
Including:
* Half-Life 2: Lost Coast
* Half-Life 2: Episode One
* Half-Life 2: Episode Two
