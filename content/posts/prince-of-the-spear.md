+++
title = "Prince of the Spear"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2018"]
artists = ["David Hair"]
series = ["Sunsurge Quartet"]
universe = ["Mage’s Blood"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 6
links = ["https://davidhairauthor.com/Books/The-Sunsurge-Quartet/Prince"]
+++
