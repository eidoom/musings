+++
title = "XCOM: Enemy Within"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2012","2013"]
artists = []
series = ["XCOM"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2015-09-12",""]]
number = [7]
#chrono =
links = ["https://en.wikipedia.org/wiki/XCOM:_Enemy_Unknown"]
+++

Expansion of *XCOM: Enemy Unknown*.
