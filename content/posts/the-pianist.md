+++
title = "The Pianist"
description = "Or, the life of a Jew in Nazi-occupied Poland"
date = 2020-10-12T14:48:59+01:00
updated = 2020-10-12T14:48:59+01:00
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["drama","historical","biography","war","world war two","nonfiction"]
years = ["2002"]
artists = []
series = []
universe = []
[extra]
consumed = [["2020-10-03"]] 
#number = 
+++

Being a pianist myself, I thought I must watch The Pianist.
The beautiful music, however, takes second stage to the attrocity of the Holocaust.
Szpilman’s struggle for survival is heart-rending.

There are unforgettable scenes which make very real this dark era of history.
In one such scene, a child attempts to crawl through a gutter to cross the wall separating the Jewish ghetto from the rest of Warsaw.
As Szpilman sees this, the child is violently attacked by an assailant on the free size of the wall.
The wall hides our sight of the violence, but what we can hear is sickening and the child is screaming.
Szpilman helps pull him through, but the child is already dead.

This film contains many moments of beauty and compassion, but is of course very painful.
Viewing this pain is important, lest we forget.
