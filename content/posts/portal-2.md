+++
title = "Portal 2"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2011"]
artists = []
series = ["Portal"]
universe = ["Half-Life"]
[extra]
active = false
published = 2022-04-19
consumed = [["2011-04-28",""]]
number = [2]
chrono = -1
links = ["https://en.wikipedia.org/wiki/Portal_2"]
+++

