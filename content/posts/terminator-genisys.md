+++
title = "Terminator Genisys"
description = "He came back"
date = 2020-11-22T14:29:50+00:00
updated = 2020-11-22T14:29:50+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["action","science fiction","adventure","comedy","time travel","fiction","speculative fiction"]
years = ["2015"]
artists = []
series = ["Terminator"]
universe = []
[extra]
consumed = [["2020-11-15"]] 
number = [5]
+++

Kyle Reese is sent to the past by his son, John Connor, to save John’s mum, Sarah Conner, who is also Kyle’s future girlfriend.
In the past.
Kyle ends up in an alternative timeline from the other *Terminator* films, and is saved by Sarah and Pops, a T-800 who is benevolent now. 
Kyle does manage to somehow contribute some memories from an alternative past though, imparted by Sarah and himself in the future to child Kyle.
Based on this information, they travel to a future which is more in the future than Sarah had initially intended, but still in the past from the perspective of the start of the film.
In this present, John has travelled from the future to his past as the human-machine aggregate avatar of Skynet and is rearing an Apple-derived machine-child called Genisys.
To stop the end of the world, our protagonists must blow some stuff up.
