+++
title = "Permutation City"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction","hard sci-fi"]
years = ["1994"]
artists = ["Greg Egan"]
series = ["Subjective Cosmology"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-02-18", "2020-03-04"]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Permutation_City"]
+++
