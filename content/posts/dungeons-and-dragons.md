+++
title = "Dungeons & Dragons 5th Edition"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["role-playing game"]
tags = ["tabletop"]
years = ["1974","2014"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-01-23",""]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Dungeons_%26_Dragons"]
+++

