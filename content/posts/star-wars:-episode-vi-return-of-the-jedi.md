+++
title = "Star Wars: Episode VI – Return of the Jedi"
description = ""
date = 2022-05-21
updated = 2022-05-21
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1983"]
artists = []
series = ["Skywalker saga","Star Wars original trilogy"]
universe = ["Star Wars"]
[extra]
active = false
published = 2022-05-21
consumed = []
number = [6,3]
chrono = 6
links = ["https://en.wikipedia.org/wiki/Return_of_the_Jedi"]
+++

