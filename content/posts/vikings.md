+++
title = "Vikings"
description = "The legacy of Ragnar Lothbrok"
date = 2020-12-05T14:52:12+00:00
updated = 2021-02-07
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","historical","drama","action","adventure"]
years = ["2013"]
artists = []
series = []
universe = []
[extra]
consumed = [["2018-11-12","2021-01-30"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Vikings_(2013_TV_series)"]
+++

This series tells the saga of the {{ wikipedia(name="Viking") }} {{ wikipedia(name="Ragnar Lothbrok") }} and his sons, inspired by real-world legend and history.
It’s a good drama, at times great.
