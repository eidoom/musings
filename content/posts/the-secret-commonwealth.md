+++
title = "The Secret Commonwealth"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2019"]
artists = ["Philip Pullman"]
series = ["The Book of Dust"]
universe = ["Northern Lights"]
[extra]
published = 2021-07-30
consumed = [["2019-12-28", "2019-12-31"]]
number = [2]
chrono = 4
links = ["https://en.wikipedia.org/wiki/The_Secret_Commonwealth"]
+++
