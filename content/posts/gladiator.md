+++
title = "Gladiator"
description = "Epic"
date = 2021-05-22T07:45:13+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["epic","historical","drama","sword and sandal","action","fiction"]
years = ["2000"]
artists = []
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-05-20"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Gladiator_(2000_film)"]
+++

It may not be the most literary but it sure is **EPIC**.
Much enjoyed.
