+++
title = "Luca"
description = "Molto buono"
date = 2021-06-27T10:23:29+01:00
updated = 2021-06-27T10:23:29+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["family friendly","italy","fantasy","bildungsroman","fiction","animated","speculative fiction"]
years = ["2021"]
artists = ["Pixar Animation Studios"]
series = []
universe = []
[extra]
published = 2021-06-27
consumed = [["2021-06-26"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Luca_(2021_film)"]
+++

