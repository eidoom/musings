+++
title = "Mother of Daemons"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2020"]
artists = ["David Hair"]
series = ["Sunsurge Quartet"]
universe = ["Mage’s Blood"]
[extra]
published = 2021-07-30
consumed = [["2020-06-03", "2020-06-09"]]
number = [4]
chrono = 8
links = ["https://davidhairauthor.com/Blog/Post/2639/MOTHER-OF-ENDINGS-FINAL-BOOKS-AND-NEW-BEGINNINGS"]
+++
