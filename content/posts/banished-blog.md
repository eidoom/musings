+++
title = "Shining Rock Software Devlog"
description = "Banished blog (and beyond)"
date = 2021-04-11T20:01:55+01:00
updated = 2021-04-11T20:01:55+01:00
draft = false
[taxonomies]
media = ["web"]
formats = ["blog"]
tags = []
years = ["2011"]
artists = ["Shining Rock Software","Luke Hodorowicz"]
series = []
universe = []
[extra]
consumed = [["2021-04-11",""]]
#number = 
#chrono =
links = ["https://www.shiningrocksoftware.com/"]
+++

Entertaining and insightful journey of a one-person computer game development team.
