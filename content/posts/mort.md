+++
title = "Mort"
description = ""
date = 2021-09-23
updated = 2021-09-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy","fiction"]
years = ["1983"]
artists = ["Terry Pratchett"]
series = ["Death (Discworld)"]
universe = ["Discworld"]
[extra]
active = false
published = 2021-09-23
consumed = [["2022-05-31","2022-06-05"]]
number = [1]
chrono = 4
links = ["https://en.wikipedia.org/wiki/Mort"]
+++

