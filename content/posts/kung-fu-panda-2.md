+++
title = "Kung Fu Panda 2"
description = ""
date = 2022-06-04
updated = 2022-06-04
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2011"]
artists = ["DreamWorks Animation"]
series = ["Kung Fu Panda"]
universe = []
[extra]
active = false
published = 2022-06-04
consumed = [["2022-06-04"]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Kung_Fu_Panda_2"]
+++

