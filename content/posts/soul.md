+++
title = "Soul"
description = "Such a lovely and hilarious film"
date = 2021-01-10T21:51:56+00:00
updated = 2021-01-10T21:51:56+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["animated","fiction","fantasy", "speculative fiction","family friendly"]
years = ["2020"]
artists = ["Pixar Animation Studios"]
series = []
universe = []
[extra]
consumed = [["2021-01-10"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Soul_(2020_film)"]
+++
