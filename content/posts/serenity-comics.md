+++
title = "Serenity"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["comic"]
tags = []
years = ["2005"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Serenity_(comics)"]
+++

Read:
* Those Left Behind
* Better Days
* The Other Half
* Float Out
* The Shepherd's Tale
* Downtime and The Other Half
* It's Never Easy
* Leaves on the Wind
* The Warrior and the Wind
* No Power in the ’Verse
