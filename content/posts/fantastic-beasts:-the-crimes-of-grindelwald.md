+++
title = "Fantastic Beasts: The Crimes of Grindelwald"
description = ""
date = 2022-06-02
updated = 2022-06-02
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2018"]
artists = []
series = ["Fantastic Beasts","Wizarding World"]
universe = []
[extra]
active = false
published = 2022-06-02
consumed = []
number = [2,10]
#chrono =
links = ["https://en.wikipedia.org/wiki/Fantastic_Beasts:_The_Crimes_of_Grindelwald"]
+++

