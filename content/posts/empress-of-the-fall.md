+++
title = "Empress of the Fall"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2017"]
artists = ["David Hair"]
series = ["Sunsurge Quartet"]
universe = ["Mage’s Blood"]
[extra]
published = 2021-07-30
consumed = [["2020-05-11", "2020-06-03"]]
number = [1]
chrono = 5
links = ["https://davidhairauthor.com/Books/The-Sunsurge-Quartet/Empress-of-the-Fall"]
+++
