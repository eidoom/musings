+++
title = "The Elder Scrolls V: Skyrim"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2011"]
artists = []
series = ["The Elder Scrolls"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2011-11-11",""]]
number = [5]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Elder_Scrolls_V:_Skyrim"]
+++

