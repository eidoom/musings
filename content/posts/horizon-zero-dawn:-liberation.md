+++
title = "Horizon Zero Dawn: Liberation"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["comic"]
tags = []
years = ["2021"]
artists = []
series = []
universe = ["Horizon: Zero Dawn"]
[extra]
active = false
published = 2022-05-05
consumed = [["2022-05-05","2022-05-06"]]
#number = []
chrono = 1.2
links = []
+++

Issues 1-4.
