+++
title = "The Galaxy, and the Ground Within"
description = ""
date = 2022-05-11
updated = 2022-05-11
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2021"]
artists = ["Becky Chambers"]
series = ["Wayfarers"]
universe = ["Galactic Commons"]
[extra]
active = false
published = 2022-05-11
consumed = [["2022-05-22","2022-05-25"]]
number = [4]
chrono = 4
links = ["https://en.wikipedia.org/wiki/The_Galaxy,_and_the_Ground_Within"]
+++

