+++
title = "Star Trek: Lower Decks"
description = "Relentless gags"
date = 2021-05-14T23:05:14+01:00
updated = 2021-10-14
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","science fiction","space","animated","cartoon","comedy"]
years = ["2020"]
artists = []
series = []
universe = ["Star Trek"]
[extra]
published = 2021-06-08
consumed = [["2021-05-14",""]]
#number = 
chrono = 6
links = ["https://en.wikipedia.org/wiki/Star_Trek:_Lower_Decks"]
+++

{{ season(number=1,started="2021-05-14",finished="2021-05-20") }}

This was very fun and funny, although I do wish it would pause to let me catch my breath.

{{ season(number=2,started="2021-09-30",finished="2021-10-14") }}

An improvement on the first season, well enjoyed!
