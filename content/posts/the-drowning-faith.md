+++
title = "The Drowning Faith"
description = ""
date = 2022-07-10
updated = 2022-07-10
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = ["grimdark","epic fantasy","fantasy","high fantasy"]
years = ["2020"]
artists = ["R. F. Kuang"]
series = ["The Poppy War Trilogy"]
universe = []
[extra]
active = false
published = 2022-07-10
consumed = [["2022-07-10"]]
number = [2.5]
#chrono =
links = []
+++

