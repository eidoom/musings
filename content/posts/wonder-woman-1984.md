+++
title = "Wonder Woman 1984"
description = "A bit ridiculous but fun"
date = 2021-01-06T11:55:10+00:00
updated = 2021-01-06T11:55:10+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["superhero","fantasy","speculative fiction","united states","fiction","america"]
years = ["2020"]
artists = []
series = []
universe = ["DC Extended Universe"]
[extra]
consumed = [["2021-01-05"]]
#number = 
chrono = 5
links = ["https://en.wikipedia.org/wiki/Wonder_Woman_1984"]
+++

