+++
title = "Rogue One: A Star Wars Story"
description = "Saddest Star Wars"
date = 2020-12-06T21:22:33+00:00
updated = 2020-12-06T21:22:33+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["science fiction","fantasy", "speculative fiction","space opera","space","fiction","epic","prequel"]
years = ["2016"]
artists = []
series = []
universe = ["Star Wars"]
[extra]
consumed = [["2020-12-06"]]
#number = 
chrono = 3.9
links = ["https://en.wikipedia.org/wiki/Rogue_One"]
+++

