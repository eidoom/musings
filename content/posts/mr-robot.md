+++
title = "Mr Robot"
description = "versus Evil Corp"
date = 2021-02-24T09:35:11+00:00
updated = 2021-03-11
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["drama","psychological","thriller","technology","united states","corporate capitalism","america"]
years = ["2015"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-02-23","2021-03-10"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Mr._Robot"]
+++

{{ season(number=1, started="2021-02-23", finished="2021-02-25", year=2015, link="https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_1_(2015)") }}

Let’s get this out for starters: what they did to Bill Harper was unforgivable.
Neither was Shayla’s fate.
Partially balanced by the existence of Gideon and Harry Goddard.

This is a story about Elliot Alderson.
He is a brilliant hacker, severely introverted, paranoid, and angry at the corporate controlled world around him.

He is clearly mentally unstable.
He even states it to us directly on many occasions.
At the start, he explains how he hears E Corp (think {{ wikipedia(name="Big Tech") }} all rolled up in one) as Evil Corp.
We proceed to hear it as Evil Corp from everyone: we’re seeing from his perspective, and he is not a reliable narrator.
This is reinforced by drug-induced sequences.

The eponymous Mr Robot is a perfectly believable character, if dangerously mad.
As time goes on, small curiosities crop up though.
They becomes increasingly insistent, like towards the end when Mr Robot turns up at Allsafe and nobody else bats an eyelid.
Then he’s Elliot’s father, and Darlene’s his sister, and he’d forgotten.
In the culmination of Elliot’s descent into madness, they pull a Fight Club on us: “You’re going to make me say it, aren’t you? I’m Mr Robot”.
I didn’t even see it coming.

Other things are obvious foreshadow in retrospect, like Darlene responding “Why shouldn’t I know where you live?” when she first picks him up from his flat, or rewatching scenes with Mr Robot and Elliot to see how the other characters behave towards “them”.
In particular, every time Elliot said or thought Mr Robot was crazy, it was just another confession.

Oh, and they pull off the hack to cripple Evil Corp.
(Go fsociety.)
Somehow...
Something’s going on with the other mad guy, Tyrell Wellick.
And the post-credit scene show that Whiterose, trans leader of the Dark Army (DA), is also some kind of Chinese business giant?
Roll season two, please.

{{ season(number=2, started="2021-2-26", finished="2021-3-1", year=2016, link="https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_2_(2016)") }}

Gideon’s gone.
Joanna Tyrell is still terrifying.
Leslie Romero’s killed by chance; Shama “Trenton” Biswas and Sunil “Mobley” Markesh are arrested but then gain amnesty, possibly via a terrified Mobley spilling the beans.
Darlene’s very valid little sister insecurities are superposed onto the position of leader of world-changing revolutionaries, then she’s caught by the FBI.
Cisco’s snuffed by the DA when the Feds make his description public.
Phillip Price continues to try to take over the world.

Dominique DiPierro’s an interesting one; she’s FBI, so she’s the enemy, but we’re rooting for her.
She has a great scene with clocks, dresses, and Whiterose in China before a submachine gun fuelled attack the next morning.
She talks to Alexa like Eliott talked to his fish, Qwerty.

Episodes start with a flashback scene to a pertinent previously unseen historical event.

Angela relentless hunts for revenge for her mother’s death.
Working at E Corp, “from within”, she seems to lose her humanity in this effort, even belittling her lovely father.

Whiterose’s alter ego is revealed as the Chinese Secretary of Security, Zhi Zhang, and she’s gearing up for Stage Two.
She has a project that has something to do with the Washington Township toxic waste scandal.
She radicalises Angela on this somehow, possibly promising the demise of E Corp?
As Qwerty’s caretaker, Angela does not appreciate the sacrifice of the fish in the water-clock.

Meanwhile, Elliot’s stay at his Mum’s is beautifully revealed as a prison stint.
Sweet and sour Ray’s actually the warden, the thugs are the guards, Leon’s a DA soldier, and the rest are regular inmates.
He makes peace with Mr Robot there, but it’s lost when they leave.
Tyrell turns up, unveils the plan—to blow up E Corp HQ with all the paper copies of the records inside—and when Elliot tries to stop it, since it’ll kill innocents, Mr Robot is one move ahead.
Tyrell shoots him with the popcorn gun.

What was the spent bullet from the popcorn gun in the Fun Society arcade about?

{{ season(number=3, started="2021-3-1", finished="2021-3-6", year=2017, link="https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_3_(2017)") }}

Introducing Irving, master of spin, revealed to be Whiterose’s past lover.
Joanna is shot by secret lover Derek.
Turns out Ernesto Santiago is a DA mole; it’s not surprising from our perspective; they’re probably threatening his mother.
Mobley and Trenton are framed as fsociety’s leaders and executed by the DA.
Leon is a chill and chilling operative.
Trump is unknowingly Whiterose’s puppet.

Darlene works for the FBI, reporting on her brother.
They grow closer, despite the context.

It’s revealed that when Elliot tried to shoot Tyrell at the arcade, the gun failed, so he recruited him instead.
Then the DA turn up and manipulate delusional Tyrell to work for them.

Angela goes cold; she believes Whiterose’s Project will save everything so becomes Elliot’s hidden handler.
She lands him a job at E Corp to manage him, but he uses the position to sabotage Stage Two while Mr Robot works nights to make it happen.

Midway through the season, we have the epic day of Stage Two.
Whiterose has pressured Price to influence the UN to allow China to annex the Congo, where she will move her Project, but she had to ask twice so the attack proceeds.
The DA has roused a rebellion and chaos ensues as protestors violently break into E Corp’s buildings.
Angela has a super tense sequence with long single-shot scenes—it’s spectacular—in which she completes a critical part of Stage Two.
Elliot fights with Mr Robot to defuse the explosion, eventually convincing him to stop it when he reveals he has successfully diverted the target: E Corp’s paper records.
The New York City premises are saved, but the 71 facilities he redirected the records to were the real target; they are destroyed with thousands killed.

After this, Angela loses it.
She’s consumed by guilt and has to believe it was for a purpose.
After plenty of foreshadow, Price reveals he’s her father and convinces her she’s been played.

Meanwhile, Elliot resolves to commit suicide but is saved by a chance day out with Mohammad, Tr3nton’s younger brother.
He then hacks the DA.

Trenton’s dead man’s switch email reveals to Elliot that the Five/Nine encryption key was logged and is on Sentinel, the FBI’s system.
Darlene seduces Dom to steal her badge to get access, but is caught.
When Darlene reveals her plan, Santiago brings her and Dom to the DA’s estate; Irving brings Elliot.
Irving axes Santiago and recruits Dom, threatening her family.
Grant Chang, Whiterose’s number two and lover, jealously wants to kill Elliot but Elliot promises to get the Project moved to the Congo if he and Darlene live, which Whiterose calls in to accept from her bathtub, watching on a tablet.
Grant is cut loose and he shoots himself—turns out he really believed sacrifice for the cause is the greatest offering.

It transpires Mr Robot had the encryption key all along, on a blank disk we initially worry will be Darlene’s deletion disk.
Also, Darlene reveals Elliot wasn’t pushed: he jumped.
Elliot and Mr Robot make amends.
Elliot undoes Five/Nine.

At this point, we have to admit our hatred of Evil Corp was blinding us to the true devils.
Bringing them down didn’t help the people on the streets.
We were initially sympathetic to Whiterose but she is increasingly exposed as the real villain.
Don’t worry, we still don’t trust Price either.

It’s clear everything that has happen was allowed, indeed desired, by the elite.
Elliot vows to destroy them.

{{ season(number=4, started="2021-3-6", finished="2021-3-10", year=2019, link="https://en.wikipedia.org/wiki/List_of_Mr._Robot_episodes#Season_4_(2019)") }}

The DA execute Angela, spurning Phillip to aid Elliot in bringing down the Deus Group.
Tyrell and Elliot get lost in the woods trying to stop a DA operative who overheard Tyrell expressing disobedience; Tyrell is shot and stumbles off to die alone.
He’s done truly despicable things but I can’t help feeling sorry for him.
Dom gets an affable handler, Janice.
We glimpse some of Whiterose’s past: she had to hide who she was in China and her true love killed himself because of it.
Magda Alderson passes away.

After Angela’s death, Elliot loses his empathy and morality, doing anything to take down Whiterose.
He plans to take her money.
He seduces Olivia Cortez, an employee of the bank holding Deus’s money, to get access.
She turns out to be someone he could love.
Regardless, since she’s a recovered addict who can only see her son if she stays clean, he drugs her to blackmail her into cooperating.
He didn’t even try talking her into it; in the end that’s the way he convinces her.

Fernando Vera returns and a theatrical play ensues in which he forces Krista Gordon to reveal what she suspects of Elliot past.
The truth is his father sexually abused him.
Elliot struggles with this revelation and Mr Robot takes over for the Deus hack.

Janice orders Dom to murder Darlene but she can’t, resulting in an epic faceoff which Dom wins, though with a knife puncturing her lung.

Through Phillip conniving a Deus meeting, Darlene and Elliot manage to hack the phones of the 100 Deus members, allowing them to complete the hostile bank transfers with MFA access.
They {{ wikipedia(name="dox", slug="doxing") }} Deus and equally distribute the funds over every Ecoin wallet.

Darlene convinces Dom to run away with her to Budapest (Cisco’s dream).
Leon’s gone freelance, so he escorts them out of the city.
Dom learns from a chance encounter with Irving that the DA are no longer a threat so decides to stay out of duty to her family, even through the FBI has her under investigation and won’t allow her to see them.
Left alone, Darlene panics and resolves to stay for Elliot.
Dom about turns and gets on the flight, missing Darlene in the bathroom.
Dom finally gets some sleep on the plane.
It seems they took what they need from each other: Dom could let go for once and Darlene could stick it out with her brother.

Elliot goes to the Washington Township power plant to destroy the Project.
The DA is one step ahead and capture him, placing him in a mindgame room like they did with Angela.
Whiterose claims everything she has done is born of love for people, to make the world a better place, to provide a fresh start.
Elliot admits he hates the world but also that people love him and that makes it worth fighting for.

What little we see of the Project resembles a particle collider.
Angela believes it will bring back the dead, Whiterose promises it will wipe away all hurt, Phillip defames it as delusional, Elliot thinks it will destroy the world.
I have no idea what Whiterose is planning.

Whiterose activates the Project and shoots herself, leaving Elliot to choose its fate.
He plays an adventure game which deactivates the Project when he chooses to stay with the friend—but is it too late?
There is an explosion.

He wakes in a perfect alternative world.
What?
What?
What?
Is the Project some sci-fi consciousness uploader to a virtual fantasy world?
Some multiverse traverser to skip to your favourite parallel universe?
It can’t be, that’s impossible.
We’re not out of the woods of Elliot’s mind yet.

It transpires that our protagonist is not the real Elliot but the Mastermind, a personality created as part of his {{ wikipedia(name="dissociative identity disorder") }} to literally change the world to protect him.
We have watched him transition from fighting to stop blowing up buildings as part of the plan, saving those lives, to someone who will remorselessly destroy innocents to achieve his goals.
He has become one of those he set out to stop:

> “a guy trying to play God without permission.”

Now, the Mastermind doesn’t want to let go of the reins.
He has fulfilled his purpose but he’s willing to kill Elliot to stay in control.

Waking in a hospital IRL, the Mastermind talks to Darlene.
He can’t let go for himself, but he loves Darlene and will let Elliot come back for her.

> “This whole time, I thought changing the world was something you did, and after you perform, something you fought for.
> I don’t know if that’s true anymore.
> What if changing the world is just about being here?
> By showing up, no matter how many times we get told we don’t belong, by staying true even when we’re ashamed into being false, by believing in ourselves even when we’re told we’re too different.
> And if we all held onto that, if we refused to budge and fall in line, if we stood our ground for long enough, just maybe the world can’t help but change around us.”

We’re a personality too: the Friend.

> “Come on. 
> This only works if you let go too.”
