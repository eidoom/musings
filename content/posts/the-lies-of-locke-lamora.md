+++
title = "The Lies of Locke Lamora"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2006"]
artists = ["Scott Lynch"]
series = ["Gentleman Bastard","Gollancz 50"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [1,6]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Lies_of_Locke_Lamora"]
+++
