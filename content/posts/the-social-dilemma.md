+++
title = "The Social Dilemma"
description = "Fair treatment of surveillance capitalism"
date = 2021-02-12T22:32:18+00:00
updated = 2021-02-26
draft = false
[taxonomies]
media = ["video"]
formats = ["film","documentary"]
tags = ["social media","internet","data mining","society","modern","technology","corporate capitalism"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-02-12"]]
#number = 
#chrono =
links = ["https://www.thesocialdilemma.com/","https://en.wikipedia.org/wiki/The_Social_Dilemma"]
+++

A bit of Friday night {{ wikipedia(name="doomscolling") }}.

The distinct lack of mention of the film’s distributer Netflix was conspicuous.

This documentary lays out the state of our modern world (see {{ wikipedia(name="surveillance capitalism") }}) with respect to its slavery to social media, how and why we got there, and what we need to think about to fix it.
In short, people did the cool thing, playing with new technology and building these platforms, understandably not realising the potential scale and therefore consequences.
Monetisation kicked in and suddenly the usual old societal flaw of “profit”, coupled with a willful rejection of ethical consideration, drove forward our social dilemma.

The dramatisation follows the effects of social media on an American family. The son, a social media user, a boy at school, is sucked into an extreme political group (really a bunch of conspiracy theorists in their social medium resonance chamber).
His sister is arrested trying to save him from violence at a rally.
This reminds us that bystanders and even vocal rejecters are equally at risk.

It contains depressing truths, and truths that could easily provoke outrage—but it’s important to remember our fellow humans, understand where they’re coming from, to react with compassion and empathy instead.

Hopefully the message is far reaching and lasting among viewers.
It’s certainly an inspiring rally call to delete your accounts.
I also hope it instigates change, unlike {{ wikipedia(name="Tristan Harris") }}’s first attempt at Google.
