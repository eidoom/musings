+++
title = "Altered Carbon: Download Blues"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["comic"]
tags = []
years = ["2019"]
artists = []
series = []
universe = ["Altered Carbon"]
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
chrono = 4
links = ["https://www.richardkmorgan.com/writing/altered-carbon-download-blues/","https://comicvine.gamespot.com/altered-carbon-download-blues-1-hc/4000-712363/"]
+++

