+++
title = "The Elder Scrolls IV: Oblivion"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2006"]
artists = []
series = ["The Elder Scrolls"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = []
number = [4]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Elder_Scrolls_IV:_Oblivion"]
+++

