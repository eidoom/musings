+++
title = "The Obelisk Gate"
description = ""
date = 2020-07-18
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2016"]
artists = ["N.K. Jemisin"]
series = ["The Broken Earth"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-07-18", "2020-07-20"]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Obelisk_Gate"]
+++
