+++
title = "Onward"
description = "Surprising hilarious take on urban fantasy"
date = 2021-06-27T20:26:25+01:00
updated = 2021-06-27T20:26:25+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["family friendly","urban","fantasy","america","united states","adventure","magic","animated","speculative fiction","fiction","urban fantasy"]
years = ["2020"]
artists = ["Pixar Animation Studios"]
series = []
universe = []
[extra]
published = 2021-06-27
consumed = [["2021-06-27"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Onward_(film)"]
+++

