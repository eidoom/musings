+++
title = "KSP Dev Diaries"
description = "Kerbal Space Program 2 blog"
date = 2021-05-03T19:54:40+01:00
updated = 2021-05-03T19:54:40+01:00
draft = false
[taxonomies]
media = ["web"]
formats = ["blog"]
tags = []
years = ["2019"]
artists = ["Squad"]
series = []
universe = []
[extra]
consumed = [["2021-04-16",""]]
#number = 
#chrono =
links = ["https://www.kerbalspaceprogram.com/category/dev-diaries/"]
+++

Interesting blog about making a rocket science game.
