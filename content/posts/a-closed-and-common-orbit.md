+++
title = "A Closed and Common Orbit"
description = ""
date = 2022-05-11
updated = 2022-05-11
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2016"]
artists = ["Becky Chambers"]
series = ["Wayfarers"]
universe = ["Galactic Commons"]
[extra]
active = false
published = 2022-05-11
consumed = [["2022-05-11","2022-05-15"]]
number = [2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/A_Closed_and_Common_Orbit"]
+++

A story about people who are not recognised as people.
Pepper's life: genetweaked labourer under the Mothers, survivalist within the wing of Owl, Port Coriol mech tech and mum to Sidra.
Sweet Blue: rejected by Enhanced Humanity, questionably human in the Galactic Commons[^clone], artist, dear companion to Pepper.
Sidra: not Lovey, sapient[^1] AI without sapient[^2] rights, housed in an illegal body kit.

We learn that the Aeluons have four sexes. Tak is a shon, who alternates between male and female.

[^clone]: Cloning is illegal.

[^1]: Sentience is awareness through senses. Sapience is having thought.

[^2]: Human would be speciest.
