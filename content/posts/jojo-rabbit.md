+++
title = "Jojo Rabbit"
description = ""
date = 2021-10-03
updated = 2021-10-03
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","drama","world war two","historical","fiction","nazism","satire","black comedy","nationalism","fascism","racism","war","bildungsroman"]
years = ["2019"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2021-10-03
consumed = [["2021-10-02"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Jojo_Rabbit"]
+++

