+++
title = "Kung Fu Panda"
description = ""
date = 2022-06-03
updated = 2022-06-03
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2008"]
artists = ["DreamWorks Animation"]
series = ["Kung Fu Panda"]
universe = []
[extra]
active = false
published = 2022-06-03
consumed = [["2022-06-03"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Kung_Fu_Panda_(film)"]
+++

