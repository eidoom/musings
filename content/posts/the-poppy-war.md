+++
title = "The Poppy War"
description = ""
date = 2022-06-11
updated = 2022-06-11
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["grimdark","epic fantasy","fantasy","high fantasy"]
years = ["2018"]
artists = ["R. F. Kuang"]
series = ["The Poppy War Trilogy"]
universe = []
[extra]
active = false
published = 2022-06-11
consumed = [["2022-06-11","2022-06-28"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Poppy_War"]
+++

