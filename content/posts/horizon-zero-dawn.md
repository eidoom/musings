+++
title = "Horizon: Zero Dawn"
description = "Post-apocalyptic tribal future with robots"
date = 2020-10-08T11:20:50+01:00
updated = 2020-10-08T11:20:50+01:00
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["adventure","rpg","story","science fiction","open world","fiction","speculative fiction"]
years = ["2020"]
artists = ["Guerrilla Games"]
series = []
universe = ["Horizon: Zero Dawn"]
[extra]
completed = true
consumed = [["2020-08-08", ""]]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Horizon_Zero_Dawn","https://www.guerrilla-games.com/play/horizon"]
+++

I love the setting: primitive societies living in the lush ruins of our existence.
I found the presence of animorphic robots to be jarring at first, but quickly accepted them as a central part of this beautiful world.
The tribes and individuals are diverse and carry real identity.
The combat, focused on identifying and exploiting the weaknesses of opponents, is clever and fun.
The stories are exciting; piecing together the history of our civilisation’s downfall with Aloy is an awe-inspiring journey.
