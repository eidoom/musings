+++
title = "Warhammer"
description = ""
date = 2022-05-06
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["miniature wargame"]
tags = []
years = ["1983"]
artists = []
series = []
universe = []
[extra]
alt = "Warhammer Fantasy Battle"
active = false
published = 2022-05-06
consumed = []
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Warhammer_(game)"]
+++

