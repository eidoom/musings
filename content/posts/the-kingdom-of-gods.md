+++
title = "The Kingdom of Gods"
description = ""
date = 2020-08-02
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2011"]
artists = ["N. K. Jemisin"]
series = ["The Inheritance Trilogy"]
universe = ["Inheritance"]
[extra]
published = 2021-07-30
consumed = [["2020-08-11", "2020-08-19"]]
number = [3]
chrono = 3
links = ["https://en.wikipedia.org/wiki/The_Kingdom_of_Gods"]
+++
