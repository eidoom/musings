+++
title = "The Lord of the Rings: The Battle for Middle-earth II"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2006"]
artists = []
series = []
universe = ["Middle-Earth"]
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
chrono = 31.32
links = ["https://en.wikipedia.org/wiki/The_Lord_of_the_Rings:_The_Battle_for_Middle-earth_II"]
+++

Including *The Lord of the Rings: The Battle for Middle-earth II: The Rise of the Witch-king*.
