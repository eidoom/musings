+++
title = "Anno 1800"
description = "Industrial revolution reskin of Anno 2070"
date = 2021-01-06T12:11:15+00:00
updated = 2021-01-06T12:11:15+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["real-time strategy","city builder","simulation","historical"]
years = ["2019"]
artists = ["Blue Byte"]
series = ["Anno"]
universe = []
[extra]
consumed = [["2020-12-15",""]]
number = [7]
#chrono =
links = ["https://en.wikipedia.org/wiki/Anno_1800","https://www.ubisoft.com/en-gb/game/anno/1800"]
+++

A peaceful playing experience, simple and tranquil.
Detailed and pretty graphics.

