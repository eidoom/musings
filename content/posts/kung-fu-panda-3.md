+++
title = "Kung Fu Panda 3"
description = ""
date = 2022-06-07
updated = 2022-06-07
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2016"]
artists = ["DreamWorks Animation"]
series = ["Kung Fu Panda"]
universe = []
[extra]
active = false
published = 2022-06-07
consumed = [["2022-06-07"]]
number = [3]
#chrono =
links = ["https://en.wikipedia.org/wiki/Kung_Fu_Panda_3"]
+++

