+++
title = "The Resistance"
description = ""
date = 2022-05-06
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = []
years = ["2009"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-05-06
consumed = []
#number = []
#chrono =
links = ["https://boardgamegeek.com/boardgame/41114/resistance"]
+++

