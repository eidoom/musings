+++
title = "The Hobbit"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1937"]
artists = []
series = []
universe = ["Middle-Earth"]
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
chrono = 30
links = ["https://en.wikipedia.org/wiki/The_Hobbit"]
+++

