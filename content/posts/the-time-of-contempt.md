+++
title = "The Time of Contempt"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1995","2013"]
artists = ["Andrzej Sapkowski","David French"]
series = ["The Witcher Saga"]
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 4
links = ["https://en.wikipedia.org/wiki/Time_of_Contempt"]
+++
