+++
title = "Altered Carbon"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2002"]
artists = ["Richard K. Morgan"]
series = ["Takeshi Kovacs"]
universe = ["Altered Carbon"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Altered_Carbon"]
+++
