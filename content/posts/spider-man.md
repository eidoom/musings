+++
title = "Spider-Man"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2002"]
artists = []
series = ["Sam Raimi’s Spider-Man"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-04-08"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Spider-Man_(2002_film)"]
+++

