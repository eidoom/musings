+++
title = "Bill & Ted Face the Music"
description = "Dude"
date = 2020-11-22T11:25:37+00:00
updated = 2020-11-22T11:25:37+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","science fiction","fiction","time travel","speculative fiction","fantasy"]
years = ["2020"]
artists = []
series = ["Bill & Ted"]
universe = []
[extra]
consumed = [["2020-11-09"]] 
number = [3]
+++

Bill and Ted as middle age fathers still have not written the great song to save the planet, with Wyld Stallyns an unpopular, mediocre band.
They travel forwards in time to attempt to plagarise the song from themselves.
