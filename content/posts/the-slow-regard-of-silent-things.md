+++
title = "The Slow Regard of Silent Things"
description = ""
date = 2022-08-14
updated = 2022-08-14
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = []
years = ["2014"]
artists = ["Patrick Rothfuss"]
series = ["The Kingkiller Chronicle"]
universe = []
[extra]
active = false
published = 2022-08-14
consumed = [["2022-08-16","2022-08-18"]]
number = [2.1]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Slow_Regard_of_Silent_Things"]
+++

