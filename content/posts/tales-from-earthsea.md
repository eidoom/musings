+++
title = "Tales from Earthsea"
description = ""
date = 2022-04-19
updated = 2022-05-02
draft = false
[taxonomies]
media = ["book"]
formats = ["collection","short story"]
tags = []
years = ["2001"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = ["Earthsea"]
[extra]
active = false
published = 2022-04-19
consumed = [["2022-04-03","2022-05-02"]]
number = [5]
chrono = 7
links = ["https://en.wikipedia.org/wiki/Tales_from_Earthsea"]
+++

{{ short(title="The Finder", years=["2001"], started="2022-04-03", finished="2022-04-04") }}

{{ short(title="Darkrose and Diamond", years=["1999"], started="2022-04-05", finished="2022-04-06") }}

{{ short(title="The Bones of the Earth", years=["2001"], started="2022-04-07", finished="2022-04-11") }}

{{ short(title="On the High Marsh", years=["2001"], started="2022-04-21", finished="2022-04-24") }}

{{ short(title="Dragonfly", years=["1997"], started="2022-05-02") }}
