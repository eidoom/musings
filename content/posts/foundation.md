+++
title = "Foundation"
description = "Pretty Banished clone"
date = 2021-05-17T17:45:46+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["city builder","medieval","simulation","indie","early access","strategy"]
years = ["2019"]
artists = ["Polymorph Games"]
series = []
universe = []
[extra]
completed = false
published = 2021-06-08
consumed = [["2021-05-17",""]]
#number = 
#chrono =
links = ["https://www.polymorph.games/"]
+++

{{ playthrough(name="Early access",started="2021-05-17",finished="2021-05-18") }}

The game is graphically gorgeous, although the UI needs some love.
It was fun to play although I soon exhausted what it currently has to offer in economy mechanics.
Promising.
