+++
title = "The Reality Dysfunction"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1996"]
artists = ["Peter F. Hamilton"]
series = ["The Night’s Dawn Trilogy"]
universe = ["Confederation universe"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 1
links = []
+++
