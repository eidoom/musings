+++
title = "Star Wars: Episode III – Revenge of the Sith"
description = ""
date = 2022-05-21
updated = 2022-05-21
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2005"]
artists = []
series = ["Skywalker saga","Star Wars prequel trilogy"]
universe = ["Star Wars"]
[extra]
active = false
published = 2022-05-21
consumed = []
number = [3,3]
chrono = 3
links = ["https://en.wikipedia.org/wiki/Star_Wars:_Episode_III_%E2%80%93_Revenge_of_the_Sith"]
+++

