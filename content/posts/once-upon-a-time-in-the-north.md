+++
title = "Once Upon a Time in the North"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = []
years = ["2008"]
artists = ["Philip Pullman"]
series = ["His Dark Materials"]
universe = ["Northern Lights"]
[extra]
published = 2021-07-30
consumed = []
number = [0.5]
chrono = 0.5
links = ["https://en.wikipedia.org/wiki/Once_Upon_a_Time_in_the_North"]
+++
