+++
title = "300: Rise of an Empire"
description = ""
date = 2021-10-02
updated = 2021-10-02
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["sequel","sword and sandal","epic","fantasy","historical","action","fiction","heroic"]
years = ["2014"]
artists = []
series = ["300"]
universe = []
[extra]
active = false
published = 2021-10-02
consumed = [["2021-10-01"]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/300%3A_Rise_of_an_Empire"]
+++

