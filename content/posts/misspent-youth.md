+++
title = "Misspent Youth"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2002"]
artists = ["Peter F. Hamilton"]
series = []
universe = ["Commonwealth universe"]
[extra]
published = 2021-07-30
consumed = []
#number = []
chrono = 1
links = []
+++
