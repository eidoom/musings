+++
title = "The Abyss Beyond Dreams"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2014"]
artists = ["Peter F. Hamilton"]
series = ["The Chronicle of the Fallers"]
universe = ["Commonwealth universe"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 7
links = []
+++
