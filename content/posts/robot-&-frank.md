+++
title = "Robot & Frank"
description = "A bittersweet little story"
date = 2021-04-25T23:02:32+01:00
updated = 2021-04-25T23:02:32+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["science fiction","comedy","drama","robot","speculative fiction","fiction"]
years = ["2012"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-04-25"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Robot_%26_Frank"]
+++

An elderly man struggles to accept his dementia.
He is forced to come to terms with his condition when he must wipe the memory of his healthcare robot—his “friend”—to destroy evidence implicating him in a crime.
