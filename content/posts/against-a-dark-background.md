+++
title = "Against a Dark Background"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["1993"]
artists = ["Iain M. Banks"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = [["2019-10-03", "2019-11-14"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Against_a_Dark_Background"]
+++
