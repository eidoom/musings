+++
title = "Horizon Zero Dawn: The Sunhawk"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["comic"]
tags = []
years = ["2020"]
artists = []
series = []
universe = ["Horizon: Zero Dawn"]
[extra]
active = false
published = 2022-05-05
consumed = [["2020-11-01","2022-05-05"]]
#number = []
chrono = 1.1
links = []
+++

Issues 0-4.
