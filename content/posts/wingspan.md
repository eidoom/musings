+++
title = "Wingspan"
description = ""
date = 2021-09-19
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game","card game"]
tags = ["strategy","engine building","animals","birds"]
years = ["2019"]
artists = ["Elizabeth Hargrave"]
series = []
universe = []
[extra]
players = [1,5]
active = false
published = 2021-09-19
consumed = [["2021-09-18"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Wingspan_(board_game)","https://stonemaiergames.com/games/wingspan/","https://boardgamegeek.com/boardgame/266192/wingspan"]
+++

I played with three players.
