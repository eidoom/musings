+++
title = "Star Wars: Episode VIII – The Last Jedi"
description = ""
date = 2020-11-29T16:56:44+00:00
updated = 2020-11-29T16:56:44+00:00
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["epic","science fiction","fantasy", "speculative fiction","space opera","space","fiction"]
years = ["2017"]
artists = []
series = ["Skywalker saga","Star Wars sequel trilogy"]
universe = ["Star Wars"]
[extra]
consumed = [["2020-11-28"]]
number = [8,2]
chrono = 8
links = ["https://en.wikipedia.org/wiki/Star_Wars:_The_Last_Jedi"]
+++

