+++
title = "Star Wars: Episode II – Attack of the Clones"
description = ""
date = 2022-05-21
updated = 2022-05-21
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2002"]
artists = []
series = ["Skywalker saga","Star Wars prequel trilogy"]
universe = ["Star Wars"]
[extra]
active = false
published = 2022-05-21
consumed = []
number = [2,2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/Star_Wars:_Episode_II_%E2%80%93_Attack_of_the_Clones"]
+++

