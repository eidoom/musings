+++
title = "Reaper’s Gale"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2007"]
artists = ["Steven Erikson"]
series = ["Malazan Book of the Fallen"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [7]
#chrono =
links = []
+++
