+++
title = "Obi-Wan Kenobi"
description = ""
date = 2022-05-21
updated = 2022-05-21
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = []
years = ["2022"]
artists = []
series = []
universe = ["Star Wars"]
[extra]
active = false
published = 2022-05-21
consumed = [["2022-05-31","2022-05-30"]]
#number = []
chrono = 3.5
links = ["https://en.wikipedia.org/wiki/Obi-Wan_Kenobi_(TV_series)"]
+++

