+++
title = "Cosmic Encounter"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = []
years = ["2008"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Cosmic_Encounter","https://boardgamegeek.com/boardgame/39463/cosmic-encounter"]
+++

