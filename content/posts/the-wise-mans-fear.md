+++
title = "The Wise Man’s Fear"
description = ""
date = 2021-07-30
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2011"]
artists = ["Patrick Rothfuss"]
series = ["The Kingkiller Chronicle"]
universe = []
[extra]
alt = "The Kingkiller Chronicle: Day Two"
active = false
published = 2021-07-30
consumed = [["2022-07-31","2022-08-14"]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Wise_Man%27s_Fear"]
+++
