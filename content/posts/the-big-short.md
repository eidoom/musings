+++
title = "The Big Short"
description = "The story behind the 2008 crash"
date = 2021-02-08T22:53:08+00:00
updated = 2021-02-08T22:53:08+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["biography","drama","comedy","united states","america"]
years = ["2015"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-02-08"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Big_Short_(film)"]
+++

Yes, this was inspired by the whole {{ wikipedia(name="GameStop thing",slug="GameStop short squeeze") }}.

A nice treatment of the {{ wikipedia(name="2008 crash", slug="Financial crisis of 2007–2008") }}.
Entertaining, informative, and utterly terrifying.
