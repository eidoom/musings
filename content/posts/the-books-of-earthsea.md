+++
title = "The Books of Earthsea"
description = ""
date = 2022-05-02
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["short story","collection"]
tags = []
years = ["2018"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = []
[extra]
alt="The Books of Earthsea: The Complete Illustrated Edition"
active = false
published = 2022-05-02
consumed = [["2021-11-20","2022-05-05"]]
number = [0]
#chrono =
links = []
+++

{{ short(title="A Description of Earthsea", started="2022-05-02") }}

{{ short(title="The Daughter of Odren", started="2022-05-02") }}

{{ short(title="Firelight", started="2022-05-03") }}

{{ short(title="Earthsea Revisioned", alt="Children, Women, Men, and Dragons", started="2022-05-04", finished="2022-05-05") }}
