+++
title = "A Quantum Murder"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1994"]
artists = ["Peter F. Hamilton"]
series = ["Greg Mandel"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [2]
#chrono =
links = []
+++
