+++
title = "Leviathan Falls"
description = ""
date = 2019-11-25
updated = 2022-04-19
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2021"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2022-04-17
consumed = [["2021-12-23","2021-12-26"]]
number = [9]
chrono = 9
links = []
+++

