+++
title = "Space Engineers"
description = ""
date = 2021-06-06T14:12:03+01:00
updated = 2021-06-06T14:12:03+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["voxel","space","sandbox","indie","early access"]
years = ["2013","2019"]
artists = ["Keen Software House"]
series = []
universe = []
[extra]
published = 2022-05-07
consumed = [["2021-05-22",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Space_Engineers","https://www.spaceengineersgame.com/","https://spaceengineerswiki.com/"]
+++

