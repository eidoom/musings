+++
title = "The Neutronium Alchemist"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1997"]
artists = ["Peter F. Hamilton"]
series = ["The Night’s Dawn Trilogy"]
universe = ["Confederation universe"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 2
links = []
+++
