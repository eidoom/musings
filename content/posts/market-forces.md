+++
title = "Market Forces"
description = ""
date = 2020-03-01
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction","thriller","dystopian"]
years = ["2004"]
artists = ["Richard K. Morgan"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-04-06", "2020-04-13"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Market_Forces"]
+++
