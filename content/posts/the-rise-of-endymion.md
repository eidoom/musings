+++
title = "The Rise of Endymion"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1997"]
artists = ["Dan Simmons"]
series = ["Hyperion Cantos"]
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
number = [4]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Rise_of_Endymion"]
+++

