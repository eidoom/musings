+++
title = "The Ocean at the End of the Lane"
description = "A dream of childhood"
date = 2021-05-03T18:38:13+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["legend","myth","fiction","fantasy","speculative fiction","magic","surrealism","magical realism","horror","dark fantasy","england","united kingdom","britain"]
years = ["2013"]
artists = ["Neil Gaiman"]
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-05-27","2021-05-30"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Ocean_at_the_End_of_the_Lane"]
+++

A lovely read.
Gaiman takes us on a compelling journey with his trademark touch of the fantastical and the humorous.
No matter how magical the tale he weaves, it *just is*.

Our narrator is a young boy who finds himself involved with some deitic happenings.
