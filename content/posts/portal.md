+++
title = "Portal"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2007"]
artists = []
series = ["Portal"]
universe = ["Half-Life"]
[extra]
active = false
published = 2022-04-19
consumed = [["2010-05-19",""]]
number = [1]
chrono = -2
links = ["https://en.wikipedia.org/wiki/Portal_(video_game)"]
+++

Spiritual successor to [*Narbacular Drop*](https://en.wikipedia.org/wiki/Narbacular_Drop).
