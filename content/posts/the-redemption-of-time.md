+++
title = "The Redemption of Time"
description = "Beyond Death’s End"
date = 2020-11-22T15:12:16+00:00
updated = 2020-11-28
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["paraquel","science fiction","fiction","speculative fiction"]
years = ["2019","2011"]
artists = ["Baoshu","Ken Liu"]
series = []
universe = ["Three-Body"]
[extra]
consumed = [["2020-11-22", "2020-11-27"]]
#number = 
chrono = 3.5
links = ["https://en.wikipedia.org/wiki/Remembrance_of_Earth%27s_Past#Extended_series"]
+++

An entertaining ode to [*Rememberance of Earth’s Past*](../../series/remembrance-of-earth-s-past/).
