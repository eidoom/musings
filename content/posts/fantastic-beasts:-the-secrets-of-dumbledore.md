+++
title = "Fantastic Beasts: The Secrets of Dumbledore"
description = ""
date = 2022-06-01
updated = 2022-06-01
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2022"]
artists = []
series = ["Fantastic Beasts","Wizarding World"]
universe = []
[extra]
active = false
published = 2022-06-01
consumed = [["2022-06-01"]]
number = [3,11]
#chrono =
links = ["https://en.wikipedia.org/wiki/Fantastic_Beasts:_The_Secrets_of_Dumbledore"]
+++

