+++
title = "Devs"
description = "Deus"
date = 2020-12-05T14:42:23+00:00
updated = 2021-02-20
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["drama","fiction","thriller","science fiction","speculative fiction","miniseries"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
consumed = [["2020-07-18","2020-07-25"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Devs"]
+++

An entertaining thought experiment and engaging drama around the psychological effects of prescience and the conundrum of free will in a deterministic universe.
