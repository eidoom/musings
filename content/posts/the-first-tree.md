+++
title = "The First Tree"
description = "and the fox’s journey"
date = 2021-05-09T22:17:57+01:00
updated = 2021-05-09T22:17:57+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["story","walking simulator","exploration","arthouse","indie"]
years = ["2017"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-05-08","2021-05-09"]]
#number = 
#chrono =
links = ["https://www.thefirsttree.com/"]
+++

A touching wee story about a man’s relationship with his recently deceased father, narrated over a dream about a fox searching for her lost cubs.
