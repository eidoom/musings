+++
title = "Slipways"
description = "Arcade 4X"
date = 2021-06-16T12:15:08+01:00
updated = 2021-06-16T12:15:08+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["space","strategy","turn-based","puzzle","sandbox","3x","tbs"]
years = ["2021"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-06-15",""]]
#number = 
#chrono =
links = ["https://slipways.net/"]
+++

Like a single-player board game of interstellar economy balancing.
Fast and fun.
