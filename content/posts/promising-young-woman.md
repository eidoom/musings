+++
title = "Promising Young Woman"
description = ""
date = 2021-10-04
updated = 2021-10-04
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["thriller","drama","rape","revenge"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2021-10-04
consumed = [["2021-10-03"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Promising_Young_Woman"]
+++

