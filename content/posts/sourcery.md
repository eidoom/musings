+++
title = "Sourcery"
description = ""
date = 2022-06-05
updated = 2022-06-05
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1988"]
artists = ["Terry Pratchett"]
series = ["Rincewind"]
universe = ["Discworld"]
[extra]
active = false
published = 2022-06-05
consumed = [["2022-06-05","2022-06-11"]]
number = [3]
chrono = 5
links = ["https://en.wikipedia.org/wiki/Sourcery"]
+++

