+++
title = "Subnautica: Below Zero"
description = "More Subnautica—excellent!"
date = 2020-11-06T10:23:27+00:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["exploration","story","crafting","survival","open world","science fiction","speculative fiction","fiction","far future"]
years = ["2019"]
artists = ["Unknown Worlds Entertainment"]
series = ["Subnautica"]
universe = []
[extra]
completed = false
published = 2022-05-07
consumed = [["2020-07-21", ""]] 
number = [2]
links = ["https://en.wikipedia.org/wiki/Subnautica:_Below_Zero","https://unknownworlds.com/subnautica/"]
+++

{{ playthrough(name="Through final beta and release", started="2021-04-18") }}
