+++
title = "Endymion"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1996"]
artists = ["Dan Simmons"]
series = ["Hyperion Cantos"]
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
number = [3]
#chrono =
links = ["https://en.wikipedia.org/wiki/Endymion_(Simmons_novel)"]
+++

