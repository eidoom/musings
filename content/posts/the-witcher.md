+++
title = "The Witcher"
description = ""
date = 2021-09-05
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["dark","fantasy","drama"]
years = ["2019"]
artists = []
series = []
universe = ["The Witcher"]
[extra]
active = false
published = 2021-09-05
consumed = [["2019-12-20",""]]
#number = []
chrono = 0.9
links = ["https://en.m.wikipedia.org/wiki/The_Witcher_(TV_series)"]
+++

{{ season(number=1, started="2019-12-20",finished="2019-12-22") }}

{{ season(number=2, started="2022-01-05",finished="2022-01-07") }}
