+++
title = "Project Hail Mary"
description = "First contact"
date = 2021-05-07T12:10:01+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fiction","speculative fiction","science fiction","hard sci-fi","near future","amnesia","space"]
years = ["2021"]
artists = ["Andy Weir"]
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-05-10","2021-05-19"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Project_Hail_Mary"]
+++

First contact is an exo-bacterium that’s eating the sun so gluttonously as to spell extinction on Earth.
Second contact is an *earie* space-spider who comes to hold a special place of affection in our heart.
Ryland’s the unwitting nerd who has to save the world.
