+++
title = "The Lord of the Rings"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2003"]
artists = []
series = []
universe = ["Middle-Earth"]
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
chrono = 31.2
links = ["https://en.wikipedia.org/wiki/The_Lord_of_the_Rings_(film_series)"]
+++

