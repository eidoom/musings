+++
title = "Magic: The Gathering"
description = ""
date = 2022-08-16
updated = 2022-08-16
draft = false
[taxonomies]
media = ["game"]
formats = ["card game"]
tags = ["tabletop"]
years = ["1993"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-08-16
consumed = [["2022-08-15"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Magic:_The_Gathering"]
+++

Played with 3 players.
