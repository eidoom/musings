+++
title = "Oath: Chronicles of Empire and Exile"
description = "Deep strategy"
date = 2021-09-01
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = ["strategy","amerigame","fantasy","political","thematic"]
years = ["2021"]
artists = []
series = []
universe = []
[extra]
players = [1,6]
active = false
published = 2021-09-01
consumed = [["2021-08-31"]]
#number = []
#chrono =
links = ["https://www.boardgamegeek.com/boardgame/291572/oath-chronicles-empire-and-exile","https://ledergames.com/products/oath-chronicles-of-empire-exile"]
+++

The banner of the darkest secret and of the people’s favour; the oathkeeper and the usurper; citizens and exiles; advisors and denizens; relics; pawns and warbands; the Cradle, Provinces and Hinterland; the imperial chancellor.
The lingo of the game flows of the tongue.

A game of intricately linked and delicate rules.
Players are asymmetric and new cards can restructure the board.
I’m interested to see how the inter-game continuation plays out.
I enjoyed the “writing history” flavour and stylised artwork.

I played with four players.
