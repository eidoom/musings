+++
title = "The Republic of Thieves"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2013"]
artists = ["Scott Lynch"]
series = ["Gentleman Bastard"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [3]
#chrono =
links = []
+++
