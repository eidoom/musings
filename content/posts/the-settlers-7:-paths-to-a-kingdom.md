+++
title = "The Settlers 7: Paths to a Kingdom"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2010"]
artists = ["Blue Byte"]
series = ["The Settlers"]
universe = []
[extra]
active = false
published = 2022-05-05
consumed = []
number = [7]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Settlers_7:_Paths_to_a_Kingdom"]
+++

