+++
title = "Black Widow"
description = "Fun"
date = 2021-07-09T22:06:09+01:00
updated = 2021-07-09T22:06:09+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["superhero","action"]
years = ["2021"]
artists = []
series = []
universe = ["Marvel Cinematic Universe"]
[extra]
published = 2021-07-09
consumed = [["2021-07-09"]]
#number = 
chrono = 24
links = ["https://en.wikipedia.org/wiki/Black_Widow_(2021_film)"]
+++

