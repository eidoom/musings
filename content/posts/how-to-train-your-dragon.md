+++
title = "How to Train Your Dragon"
description = "Delightful"
date = 2021-01-06T11:48:12+00:00
updated = 2021-01-06T11:48:12+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["fantasy", "speculative fiction","dragon","viking","animated","family friendly"]
years = ["2010"]
artists = ["DreamWorks Animation"]
series = []
universe = []
[extra]
consumed = [["2020-12-30"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/How_to_Train_Your_Dragon_(film)"]
+++

This film is a joy to watch.
