+++
title = "The Tombs of Atuan"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1970"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = ["Earthsea"]
[extra]
active = false
published = 2022-04-19
consumed = [["2021-12-27","2022-01-23"]]
number = [1]
chrono = 3
links = ["https://en.wikipedia.org/wiki/The_Tombs_of_Atuan"]
+++

