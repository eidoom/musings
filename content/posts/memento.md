+++
title = "Memento"
description = "What am I doing here, again?"
date = 2020-11-04T15:25:42+00:00
updated = 2020-11-07
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["thriller","mystery","psychological","neo-noir","fiction","amnesia"]
years = ["2000"]
artists = ["Christopher Nolan","Guy Pearce"]
series = []
universe = []
[extra]
consumed = [["2020-11-03"]] 
#number = 
+++

Leonard suffers from {{ wikipedia(name="anterograde amnesia") }} brought about by the murder of his wife.
He uses photographs and notes to himself, including tattoos, in place of a working memory to function and hunt the killer.
How do you present a film from the perspective of a character with short-term memory loss?
Present the scenes in reverse order, with a little chaotic overlap thrown in.

The backward flow of time is immediately clear from the entrancing opening scene of a Polaroid image undeveloping.
Echoes from the past foreshadow coming revelations as we follow back the timeline.
No one can be trusted, particularly not our protagonist: he doesn’t remember.

*Remember Sammy Jankis.*
