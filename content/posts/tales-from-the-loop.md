+++
title = "Tales from the Loop"
description = "Make the impossible possible"
date = 2020-10-08T12:29:32+01:00
updated = 2020-11-07
draft = false
[taxonomies]
years = ["2020"]
media = ["video"]
formats = ["television"]
tags = ["science fiction","drama","fiction","speculative fiction"]
[extra]
consumed = [["2020-07-12", "2020-07-19"]]
+++

This is a collection of short stories all based around the town of Mercer, Ohio.
The pool of characters is explored through the episodes in an interesting manner that shows how their lives intersect.
The series has a quiet, melancholic feeling to it.

The aesthetic identity is strong; it feels Scandinavian with retro-future tech.
