+++
title = "The Hydrogen Sonata"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2012"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = [["2019-08-21", "2019-10-03"]]
number = [10]
chrono = 8
links = ["https://en.wikipedia.org/wiki/The_Hydrogen_Sonata"]
+++
