+++
title = "Inversions"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1998"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = []
number = [6]
chrono = 10
links = ["https://en.wikipedia.org/wiki/Inversions_(novel)"]
+++
