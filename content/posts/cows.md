+++
title = "Great Western Trail"
description = "“Cows”"
date = 2021-05-03T18:46:19+01:00
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = ["point salad","strategy","animals","economic","western","cattle"]
years = ["2016"]
artists = []
series = []
universe = []
[extra]
players = [2,4]
consumed = [["2021-04-03"]]
#number = 
#chrono =
links = ["https://boardgamegeek.com/boardgame/193738/great-western-trail"]
+++

Plenty of different ways to earn victory points, yet presented in an intuitive manner using consistent iconography.
Fun to play while managing diverse strategies and tripping up your opponents, of course.

I played with three players.
