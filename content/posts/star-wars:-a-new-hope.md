+++
title = "Star Wars: Epsiode IV – A New Hope"
description = "Original Star Wars"
date = 2021-03-20T22:27:43+00:00
updated = 2021-03-20T22:27:43+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["speculative fiction","fantasy","science fiction","space opera","epic","space","fiction"]
years = ["1977"]
artists = []
series = ["Skywalker saga", "Star Wars original trilogy"]
universe = ["Star Wars"]
[extra]
consumed = [["2021-03-20"]]
number = [4, 1]
chrono = 4
links = ["https://en.wikipedia.org/wiki/Star_Wars_(film)"]
+++

