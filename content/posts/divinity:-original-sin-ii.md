+++
title = "Divinity: Original Sin II"
description = ""
date = 2021-07-15T08:54:29+01:00
updated = 2021-07-15T08:54:29+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["rpg","fantasy"]
years = ["2017"]
artists = []
series = ["Divinity: Original Sin"]
universe = ["Divinity"]
[extra]
completed = false
active = true
published = 2022-05-07
consumed = [["2021-07-15",""]]
number = [2]
chrono = 5
links = ["https://en.wikipedia.org/wiki/Divinity:_Original_Sin_II"]
+++

