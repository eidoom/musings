+++
title = "The Lightning Tree"
description = ""
date = 2022-08-14
updated = 2022-08-14
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = []
years = ["2014"]
artists = ["Patrick Rothfuss"]
series = ["The Kingkiller Chronicle","Rogues"]
universe = []
[extra]
active = false
published = 2022-08-14
consumed = [["2022-08-14","2022-08-16"]]
number = [0.9,20]
#chrono =
links = []
+++

