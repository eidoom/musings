+++
title = "A Psalm for the Wild-Built"
description = ""
date = 2022-05-25
updated = 2022-05-25
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = ["solarpunk","science fiction"]
years = ["2022"]
artists = ["Becky Chambers"]
series = ["Monk & Robot"]
universe = []
[extra]
active = false
published = 2022-05-25
consumed = [["2022-08-24"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/A_Psalm_for_the_Wild-Built"]
+++

