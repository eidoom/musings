+++
title = "RimWorld"
description = "Firefly-esque colony sim"
date = 2020-11-22T22:34:42+00:00
updated = 2020-11-25
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["simulator","construction and management sim","base builder","colony","speculative fiction","science fiction","drama","story","survival","strategy","indie"]
years = ["2013","2018"]
artists = ["Ludeon Studios"]
series = []
universe = []
[extra]
consumed = [["2015-10-07", ""]] 
#number = 
links = ["https://en.wikipedia.org/wiki/RimWorld","https://rimworldgame.com/"]
+++

A rich story generator.
Often produces hilarious circumstances.
Punishingly merciless at delivering setbacks.
Very fun, just don’t get too attached to anyone or you’re going to rage quit at some point.
