+++
title = "Ra"
description = "Physics, Chemistry, Biology, and Magic"
date = 2021-02-21T13:06:10+00:00
updated = 2021-02-21T13:06:10+00:00
draft = false
[taxonomies]
media = ["book","web"]
formats = ["serial"]
tags = ["science fiction","magic","speculative fiction","fiction"]
years = ["2011"]
artists = ["Sam Hughes"]
series = []
universe = []
[extra]
consumed = [["2021-02-20","2021-02-26"]]
#number = 
#chrono =
links = ["https://qntm.org/ra"]
+++

My first experience of a {{ wikipedia(name="web serial", slug="Web_fiction#Web_serial") }}; I enjoyed seeing the author’s comments, appendices, and discarded draft ending.

The concept of magic as just another boring old field of science is great.
Uncovering the lore of this world, we discover it’s a {{ wikipedia(name="Clarke’s third law", slug="Clarke’s_three_laws") }} flavoured magic, which is also great.
The magic system is full of programming references too.

The story follows sisters Laura and Natalie Ferno as they master magic and discover that life on Earth in the present day is a recreation of the world after Actual Humanity (physical humans) very nearly lost a war against Virtual Humanity involving an endgame technological wish-fulfillment system called Ra in the real far future. The Virtuals want to dedicate the entire energy-mass budget of the solar system to their hardware—it’s an age-old resource war, and, of course, it’s not over yet.
