+++
title = "Strange Dogs"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = []
years = ["2017"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [6.5]
chrono = 6.5
links = []
+++
