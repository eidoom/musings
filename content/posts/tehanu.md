+++
title = "Tehanu"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1990"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = ["Earthsea"]
[extra]
active = false
published = 2022-04-19
consumed = [["2022-03-12","2022-04-02"]]
number = [4]
chrono = 6
links = ["https://en.wikipedia.org/wiki/Tehanu"]
+++

