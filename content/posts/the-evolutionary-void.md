+++
title = "The Evolutionary Void"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2009"]
artists = ["Peter F. Hamilton"]
series = ["The Void Trilogy"]
universe = ["Commonwealth universe"]
[extra]
published = 2021-07-30
consumed = []
number = [3]
chrono = 6
links = []
+++
