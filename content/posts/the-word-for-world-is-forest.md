+++
title = "The Word for World Is Forest"
description = "Slavery of a new land and people, leading to their uprising"
date = 2021-06-19T15:37:39+01:00
updated = 2021-08-07
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = ["speculative fiction","science fiction","fiction","far future","fantasy"]
years = ["1972","1976"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
published = 2021-06-27
consumed = [["2021-06-19","2021-06-22"]]
number = [4]
chrono = 2
links = ["https://en.wikipedia.org/wiki/The_Word_for_World_Is_Forest"]
+++

> “...slavery never worked. It was uneconomical.”

A powerful read.

A prequel to the preceding Cycle stories, coinciding with the admittance of Earth (Terra) to the League of All Worlds and subsequent receiving of ansibles.

> “Right, but this isn’t slavery...”
