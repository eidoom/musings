+++
title = "Tortuga 1667"
description = ""
date = 2022-08-10
updated = 2022-08-10
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = []
years = ["2017"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-08-10
consumed = [["2022-08-10"]]
#number = []
#chrono =
links = ["https://boardgamegeek.com/boardgame/218530/tortuga-1667"]
+++

Played with 5 players.
