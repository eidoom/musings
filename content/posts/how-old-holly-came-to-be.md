+++
title = "How Old Holly Came to Be"
description = ""
date = 2022-08-14
updated = 2022-08-14
draft = false
[taxonomies]
media = ["book"]
formats = ["short story"]
tags = []
years = ["2013"]
artists = ["Patrick Rothfuss"]
series = ["The Kingkiller Chronicle","Unfettered"]
universe = []
[extra]
active = false
published = 2022-08-14
consumed = [["2022-08-14"]]
number = [0.1,2]
#chrono =
links = []
+++

