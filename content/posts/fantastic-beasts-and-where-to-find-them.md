+++
title = "Fantastic Beasts and Where to Find Them"
description = ""
date = 2022-06-02
updated = 2022-06-02
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2016"]
artists = []
series = ["Fantastic Beasts","Wizarding World"]
universe = []
[extra]
active = false
published = 2022-06-02
consumed = []
number = [1,9]
#chrono =
links = ["https://en.wikipedia.org/wiki/Fantastic_Beasts_and_Where_to_Find_Them_(film)"]
+++

