+++
title = "Ascendant’s Rite"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2015"]
artists = ["David Hair"]
series = ["Moontide Quartet"]
universe = ["Mage’s Blood"]
[extra]
published = 2021-07-30
consumed = []
number = [4]
chrono = 4
links = []
+++
