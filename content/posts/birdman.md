+++
title = "Birdman or (The Unexpected Virtue of Ignorance)"
description = "A take on superhero movies"
date = 2021-04-11T22:25:36+01:00
updated = 2021-04-11T22:25:36+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["drama"]
years = ["2014"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-04-11"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Birdman_(film)"]
+++

Great long takes.
Superb drum track.

The character conflict and drama is intense.
Riggan’s crazy hallucinations keep us on our toes.
The film calls out its own playing with realism.
