+++
title = "Dune"
description = ""
date = 2021-10-24
updated = 2021-10-24
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1984"]
artists = ["David Lynch"]
series = []
universe = ["Dune"]
[extra]
active = false
published = 2021-10-24
consumed = []
#number = []
chrono = 1.1
links = ["https://en.wikipedia.org/wiki/Dune_(1984_film)"]
+++

