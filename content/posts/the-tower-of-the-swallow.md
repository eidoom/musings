+++
title = "The Tower of the Swallow"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1997","2016"]
artists = ["Andrzej Sapkowski", "David French"]
series = ["The Witcher Saga"]
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
number = [4]
chrono = 6
links = ["https://en.wikipedia.org/wiki/The_Tower_of_the_Swallow"]
+++
