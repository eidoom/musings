+++
title = "Hyperion"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1989"]
artists = ["Dan Simmons"]
series = ["Hyperion Cantos","Gollancz 50"]
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
number = [1,8]
#chrono =
links = ["https://en.wikipedia.org/wiki/Hyperion_(Simmons_novel)"]
+++

