+++
title = "Gwent: The Witcher Card Game"
description = "“Wouldn’t mind a few rounds of cards.”"
date = 2021-06-26T08:46:44+01:00
updated = 2021-06-26T08:46:44+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["card game","collectible card game"]
years = ["2018"]
artists = ["CD Projekt Red"]
series = []
universe = []
[extra]
consumed = [["2021-06-25",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Gwent%3A_The_Witcher_Card_Game"]
+++

I’ve never really played card deck games before but I now understand the appeal.
A bit of fun with friends in a familiar setting.
