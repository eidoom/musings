+++
title = "The Tale of the Princess Kaguya"
description = "A work of art"
date = 2021-11-09
updated = 2021-11-09
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["historical","anime","animated","fantasy","japanese"]
years = ["2013"]
artists = ["Studio Ghibli"]
series = []
universe = []
[extra]
alt = "	かぐや姫の物語"
active = false
published = 2021-11-09
consumed = [["2021-11-09"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Tale_of_the_Princess_Kaguya_(film)"]
+++

