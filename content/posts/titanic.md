+++
title = "Titanic"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1997"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-04-15"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Titanic_(1997_film)"]
+++

Watched on the 110th anniversary of its sinking.
