+++
title = "The Name of the Wind"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fiction","speculative fiction","story within a story","fantasy","heroic fantasy","epic fantasy"]
years = ["2007"]
artists = ["Patrick Rothfuss"]
series = ["The Kingkiller Chronicle","Gollancz 50"]
universe = []
[extra]
alt = "The Kingkiller Chronicle: Day One"
active = false
published = 2021-07-30
consumed = [["2022-07-24","2022-07-31"]]
number = [1,9]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Name_of_the_Wind"]
+++
