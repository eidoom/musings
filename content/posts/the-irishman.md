+++
title = "The Irishman"
description = "The story of Frank Sheeran"
date = 2021-11-07
updated = 2021-11-07
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["biography","crime","epic","drama"]
years = ["2019"]
artists = ["Martin Scorsese"]
series = []
universe = []
[extra]
alt = "I Heard You Paint Houses"
active = false
published = 2021-11-07
consumed = [["2021-11-06"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Irishman"]
+++

This ticks all the boxes to be a film that I like, and I think it is a good film, but I didn’t enjoy watching it all that much.
Perhaps I was in the wrong frame of mind, maybe it was too soon after [True Detective Season 3](@/posts/true-detective.md#season-3).
