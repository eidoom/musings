+++
title = "The Expanse"
description = "Best sci-fi on TV"
date = 2021-01-06T12:35:15+00:00
updated = 2021-01-08
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["science fiction","hard sci-fi","drama","space opera","mystery","thriller","horror","speculative fiction"]
years = ["2015"]
artists = []
series = []
universe = ["The Expanse"]
[extra]
consumed = [["2017-10-01",""]]
#number = 
chrono = 100
links = ["https://en.wikipedia.org/wiki/The_Expanse_(TV_series)"]
+++

{{ season(number=5, started="2021-01-04", finished="2021-02-06", year=2020) }}

Wow, a great season.

Enjoyed the [aftershow](https://www.youtube.com/playlist?list=PLWz2DO39R-NU5FW-aFfilRvyeg9oXMTgp) from [Ty and Wes](https://tyandthatguy.com/) too.

{{ season(number=6, started="2022-01-16", finished="2022-01-16", year=2021) }}
