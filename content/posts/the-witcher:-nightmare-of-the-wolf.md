+++
title = "The Witcher: Nightmare of the Wolf"
description = "Vesemir’s story"
date = 2021-09-05
updated = 2021-09-05
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["dark","fantasy","anime","animated"]
years = ["2021"]
artists = []
series = []
universe = ["The Witcher"]
[extra]
active = false
published = 2021-09-05
consumed = [["2021-09-05"]]
#number = []
chrono = 0
links = ["https://en.wikipedia.org/wiki/The_Witcher%3A_Nightmare_of_the_Wolf"]
+++

