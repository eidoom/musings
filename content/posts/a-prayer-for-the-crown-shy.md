+++
title = "A Prayer for the Crown-Shy"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = ["solarpunk","science fiction"]
years = ["2022"]
artists = ["Becky Chambers"]
series = ["Monk & Robot"]
universe = []
[extra]
active = false
published = 2022-08-23
consumed = [["2022-08-24"]]
number = [2]
#chrono =
links = []
+++

