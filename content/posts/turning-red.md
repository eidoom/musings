+++
title = "Turning Red"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2022"]
artists = ["Pixar Animation Studios"]
series = []
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-03-15"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Turning_Red"]
+++

