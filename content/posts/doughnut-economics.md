+++
title = "Doughnut Economics"
description = "Principles for a sustainable future"
date = 2021-03-15T11:10:50+00:00
updated = 2021-03-15T11:10:50+00:00
draft = false
[taxonomies]
media = ["web"]
formats = ["website"]
tags = ["economics","society","ecology","environment","factual"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-03-15"]]
#number = 
#chrono =
links = ["https://doughnuteconomics.org/about-doughnut-economics"]
+++

I like this framework.
It takes the two biggest problems of the modern world—social division and the ecological crisis, which have always felt like two faces of the same coin to me—and paints them as the floor and ceiling of a planetary economy.
