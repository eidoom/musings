+++
title = "Hellblade: Senua’s Sacrifice"
description = "Disturbing"
date = 2021-01-06T12:32:00+00:00
updated = 2021-01-06T12:32:00+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["dark","fantasy","speculative fiction","dark fantasy","fiction","celtic","scotland","norse"]
years = ["2017"]
artists = ["Ninja Theory"]
series = []
universe = []
[extra]
completed = false
published = 2022-05-07
consumed = [["2021-01-10",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Hellblade:_Senua%27s_Sacrifice","https://www.hellblade.com/"]
+++

