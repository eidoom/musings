+++
title = "Emergency Skin"
description = "Earth cannot be saved"
date = 2020-10-12T16:06:38+01:00
updated = 2020-10-12T16:06:38+01:00
[taxonomies]
media = ["book"]
formats = ["short story","novelette"]
tags = ["science fiction","fiction","speculative fiction"]
years = ["2019"]
artists = ["N. K. Jemisin"]
series = ["Forward"]
universe = []
[extra]
consumed = [["2020-09-26"]] 
number = [5]
links = ["https://en.wikipedia.org/wiki/N._K._Jemisin#Short_stories"]
+++

The world’s richest men, only a tiny sliver of the entire population, band together to start an exoplanet colony, escaping the dying Earth and ensuring humanity’s survival.

It was easy for the rest of us to fix our planet after they left.
