+++
title = "The Light Fantastic"
description = ""
date = 2021-09-23
updated = 2021-09-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy","fiction"]
years = ["1983"]
artists = ["Terry Pratchett"]
series = ["Rincewind"]
universe = ["Discworld"]
[extra]
active = false
published = 2021-09-23
consumed = [["2022-05-29","2022-05-31"]]
number = [2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/The_Light_Fantastic"]
+++

