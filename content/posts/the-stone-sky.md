+++
title = "The Stone Sky"
description = ""
date = 2020-07-20
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2017"]
artists = ["N.K. Jemisin"]
series = ["The Broken Earth"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-07-20", "2020-07-25"]]
number = [3]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Stone_Sky"]
+++
