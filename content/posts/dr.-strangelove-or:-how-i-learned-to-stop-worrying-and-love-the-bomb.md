+++
title = "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb"
description = "Cold war satire"
date = 2021-06-24T09:24:54+01:00
updated = 2021-06-24T09:24:54+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","satire","political","black comedy","cold war","america","united states"]
years = ["1964"]
artists = ["Stanley Kubrick"]
series = []
universe = []
[extra]
consumed = []
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Dr._Strangelove"]
+++

