+++
title = "The Proposal"
description = "Funny, if dated at times"
date = 2021-01-24T20:32:28+00:00
updated = 2021-02-07
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","drama","romance","united states","america"]
years = ["2009"]
artists = []
series = []
universe = []
[extra]
consumed = [["2021-01-24"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Proposal_(2009_film)"]
+++

Outside my usual repertoire, I know.
Some jokes didn’t age well, but a laugh overall.
