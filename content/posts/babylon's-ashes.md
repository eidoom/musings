+++
title = "Babylon’s Ashes"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2016"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [6]
chrono = 6
links = ["https://en.wikipedia.org/wiki/Babylon%27s_Ashes"]
+++
