+++
title = "The Mandalorian"
description = "and Baby Yoda, of course"
date = 2020-12-05
updated = 2021-01-10
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["science fiction","fantasy", "speculative fiction","space opera","space","fiction"]
years = ["2019"]
artists = []
series = []
universe = ["Star Wars"]
[extra]
consumed = [["2019-12-12", ""]]
#number = 
chrono = 6.5
links = ["https://en.wikipedia.org/wiki/The_Mandalorian"]
+++

