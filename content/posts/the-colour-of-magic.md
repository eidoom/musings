+++
title = "The Colour of Magic"
description = ""
date = 2021-09-23
updated = 2021-09-23
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy","fiction"]
years = ["1983"]
artists = ["Terry Pratchett"]
series = ["Rincewind"]
universe = ["Discworld"]
[extra]
active = false
published = 2021-09-23
consumed = [["2022-05-27","2022-05-28"]]
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/The_Colour_of_Magic"]
+++

