+++
title = "Thirteen"
description = ""
date = 2020-03-01
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2007"]
artists = ["Richard K. Morgan"]
series = []
universe = ["Genetically modified"]
[extra]
published = 2021-07-30
consumed = [["2020-04-24", "2020-05-11"]]
#number = 
chrono = 1
links = ["https://en.wikipedia.org/wiki/Black_Man_(novel)"]
+++
