+++
title = "Solo: A Star Wars Story"
description = ""
date = 2020-12-05T10:33:12+00:00
updated = 2020-12-05T10:33:12+00:00
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["science fiction","fantasy", "speculative fiction","space opera","space","fiction","backstory","origin"]
years = ["2018"]
artists = []
series = []
universe = ["Star Wars"]
[extra]
consumed = [["2020-12-04"]]
#number = 
chrono = 3.6
links = ["https://en.wikipedia.org/wiki/Solo:_A_Star_Wars_Story"]
+++

