+++
title = "Control"
description = ""
date = 2021-01-06T12:26:41+00:00
updated = 2021-01-06T12:26:41+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["action","adventure","paranormal"]
years = ["2019"]
artists = ["Remedy Entertainment"]
series = []
universe = []
[extra]
published = 2022-05-07
completed = false
consumed = [["2020-12-14",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Control_(video_game)","https://www.remedygames.com/games/control/"]
+++

