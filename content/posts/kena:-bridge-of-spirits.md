+++
title = "Kena: Bridge of Spirits"
description = ""
date = 2021-10-24
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2021"]
artists = ["Ember Lab"]
series = []
universe = []
[extra]
active = false
completed = false
published = 2021-10-24
consumed = [["2021-10-24",""]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Kena:_Bridge_of_Spirits"]
+++

