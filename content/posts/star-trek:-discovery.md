+++
title = "Star Trek: Discovery"
description = "Michael Burnham takes on the galaxy"
date = 2020-12-05
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","speculative fiction","science fiction","action","adventure","drama","space","future","aliens"]
years = ["2017"]
artists = []
series = []
universe = ["Star Trek"]
[extra]
consumed = [["2017-09-24",""]]
#number = 
chrono = 7
links = ["https://en.wikipedia.org/wiki/Star_Trek:_Discovery"]
+++

{{ season(number=3, started="2020-10-15", finished="2020-01-07", link="https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_3)") }}

A great take of the far far future: a dystopian power struggle in the wake of the collapse of Starfleet.
Plenty of delightful silliness too.

{{ season(number=4, started="2021-11-18", finished="2022-03-19", link="https://en.wikipedia.org/wiki/Star_Trek:_Discovery_(season_4)") }}
