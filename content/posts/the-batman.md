+++
title = "The Batman"
description = "The Battinson"
date = 2022-05-15
updated = 2022-05-15
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2022"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-05-15
consumed = [["2022-05-14"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Batman_(film)"]
+++

Another night, another spectacular cinematic experience with a nihilistic lead dude.
