+++
title = "Nexus Uprising"
description = ""
date = 2019-12-26
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2017"]
artists = ["Jason M. Hough", "K. C. Alexander"]
series = ["Mass Effect: Andromeda"]
universe = ["Mass Effect"]
[extra]
published = 2021-07-30
consumed = [["2019-12-06", "2019-12-27"]]
number = [1]
chrono = 3.6
links = ["https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda_(book_series)#Nexus_Uprising"]
+++
