+++
title = "The Sound of Music"
description = "A classic"
date = 2021-01-06T11:48:32+00:00
updated = 2021-01-06T11:48:32+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["musical","drama"]
years = ["1965"]
artists = []
series = []
universe = []
[extra]
consumed = [["2020-12-31"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Sound_of_Music_(film)"]
+++

Considering I’m not so well versed in musicals or older films, I was surprised how much I enjoyed this.
