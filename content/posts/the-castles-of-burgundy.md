+++
title = "The Castles of Burgundy"
description = ""
date = 2021-09-20
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = ["strategy","eurogame","medieval","point salad","territory building","dice"]
years = ["2019"]
artists = ["Stefan Feld"]
series = []
universe = []
[extra]
players = [1,4]
active = false
published = 2021-09-20
consumed = [["2021-09-19"],["2021-09-26"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Castles_of_Burgundy","https://www.boardgamegeek.com/boardgame/271320/castles-burgundy"]
+++

The 2019 edition includes eight expansions over the original 2011 version.

I played with three players.
