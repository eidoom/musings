+++
title = "Marriage Story"
description = "Divorce"
date = 2021-09-03
updated = 2021-09-03
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["drama"]
years = ["2019"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2021-09-03
consumed = [["2021-09-03"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Marriage_Story"]
+++

As we follow Nicole and Charlie through their divorce, we explore their failed relationship.
