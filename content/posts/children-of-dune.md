+++
title = "Children of Dune"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1976"]
artists = ["Frank Herbert"]
series = ["Dune Chronicles"]
universe = ["Dune"]
[extra]
published = 2021-07-30
consumed = []
number = [3]
chrono = 3
links = ["https://en.wikipedia.org/wiki/Children_of_Dune"]
+++
