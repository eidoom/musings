+++
title = "Princess Mononoke"
description = ""
date = 2022-04-30
updated = 2022-04-30
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["1997"]
artists = ["Studio Ghibli"]
series = []
universe = []
[extra]
alt = "もののけ姫"
active = false
published = 2022-04-30
consumed = [["2022-04-28"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Princess_Mononoke"]
+++

