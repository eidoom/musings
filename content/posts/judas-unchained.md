+++
title = "Judas Unchained"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2005"]
artists = ["Peter F. Hamilton"]
series = ["Commonwealth Saga"]
universe = ["Commonwealth universe"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 3
links = []
+++
