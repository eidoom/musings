+++
title = "A Plague Tale: Innocence"
description = "Flee the rats, and worse"
date = 2020-11-25T09:52:56+00:00
updated = 2020-11-25T09:52:56+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["stealth","story","horror","historical","survival","action","adventure"]
years = ["2019"]
artists = ["Asobo Studio"]
series = []
universe = []
[extra]
completed = false
consumed = [["2020-11-25", ""]] 
#number = 
links = ["https://en.wikipedia.org/wiki/A_Plague_Tale:_Innocence","https://aplaguetale.com/"]
+++

This game is **heavy**.
