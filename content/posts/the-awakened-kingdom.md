+++
title = "The Awakened Kingdom"
description = ""
date = 2020-08-02
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = []
years = ["2014"]
artists = ["N. K. Jemisin"]
series = []
universe = ["Inheritance"]
[extra]
published = 2021-07-30
consumed = [["2020-08-19", "2020-08-20"]]
#number = []
chrono = 3.5
links = []
+++
