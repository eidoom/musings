+++
title = "The Eye of the Heron"
description = "Reflections from the still surface of a tree-ring pool"
date = 2021-08-09
updated = 2021-08-18
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction","fiction","speculative fiction","pacifism","anarchy","gender","society","feminism","taoism"]
years = ["1978"]
artists = ["Ursula K. Le Guin"]
series = []
universe = ["The Ekumen"]
[extra]
active = false
published = 2021-08-18
consumed = [["2021-08-12","2021-08-17"]]
#number = []
chrono = 1.5
links = ["https://en.wikipedia.org/wiki/The_Eye_of_the_Heron"]
+++

This one has the passion of [*The Word for World is Forest*](@/posts/the-word-for-world-is-forest.md) and the pain of [the tales of Yeowe and Werel](@/posts/five-ways-to-forgiveness.md).
It feels to me to belong to the loose universe of [The Ekumen](../../universe/the-ekumen).
It reflects on gender, militarism, and interaction between antithetical societies.

The story is set on the planet Victoria, a penal colony of Earth and completely ignored by the home planet.
There are two establishments: Victoria City, a violent, misogynistic oligarchy inhabited by the descendants of the original prison ships, criminals from Brasil-America; and Shantih Town[^1], a pacifist, anarchist people from a more recent group of global political exiles who rejected War in favour of Peace.
The two towns cooperate to survive, but Victoria City is increasingly exploiting the arrangement and becoming aggressive.

We follow Luz Marina Falco Cooper—suppressed daughter of Luis Burnier Falco, one of the top Bosses of the City—who runs away to Shantih Town to warn them of her father’s plans against them.
She turns her energy, born of long resentment of her treatment, to the escape of not just herself but all of the Town from the City after befriending Vera Adelson, a preeminent member of the Town, who is imprisoned at Casa Falco.
From Shantih Town, we accompany Lev Shults, a charismatic young man who leads passionate but peaceful protest against the actions of the City.

Luis Falco blatantly uses young Herman Macmilan, a stereotype of City society, to raise an army.
Luis commands them to incite Shantih Town to violence so they can, in retribution, enslave the Town and use them to work *{{ wikipedia(name="latifundia") }}*.
Their relationship is tense with disrespect, arrogance, and ambition.
Lev’s reciprocal civil disobedience pushes things too far and this maelstrom of masculine egotism erupts with the City army firing on a gathering of the Town; Herman fatally shoots Lev while Luis kills Herman.

This action takes second place to a time of reflection and healing.
Our female hero, Luz, assumes centre stage.
She is our ethnographer in the tight-knit community of the Town, but she also leads us on an intimate journey of self-discovery.
She inspires the Town to voyage out into the untouched alien wilderness to make a new home.

There are quiet moments in this book to be cherished: when we take the time to stop and gaze upon the wonders of Victoria, to indulge our xenophilia.
The depiction of the tree-ring, seeded in cycle by the burst of a ringtree, with its peaceful centre pool and mysterious titular heron indwellers, are beautiful.
The aboriginal fauna, playful wotsits, lazy coneys, familial pouchbats, and colourful farfallies, are largely indifferent to humans and all share an inability to live in a cage.

[^1]: *Shanti* means peace in {{ wikipedia(name="Sanskrit") }}. City folk pejoratively call their neighbour Shanty Town.
