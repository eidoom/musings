+++
title = "Viticulture: Essential Edition"
description = ""
date = 2021-09-22
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = ["strategy","economic","farming","wine"]
years = ["2015"]
artists = []
series = []
universe = []
[extra]
players = [1,6]
active = false
published = 2021-09-22
consumed = [["2021-09-21"],["2022-01-01"]]
#number = []
#chrono =
links = ["https://boardgamegeek.com/boardgame/183394/viticulture-essential-edition"]
+++

Reimplementation of 2013 *Viticulture*.

I played with three players.
