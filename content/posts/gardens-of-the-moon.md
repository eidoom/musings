+++
title = "Gardens of the Moon"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1999"]
artists = ["Steven Erikson"]
series = ["Malazan Book of the Fallen"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [1]
#chrono =
links = []
+++
