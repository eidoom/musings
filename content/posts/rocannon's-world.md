+++
title = "Rocannon’s World"
description = "Tolkienesque but also Einsteinian"
date = 2021-06-02T12:41:29+01:00
updated = 2021-07-01
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction","science fiction","fiction","far future","fantasy","epic","heroic","romantic"]
years = ["1966"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
published = 2021-06-08
consumed = [["2021-05-30","2021-06-02"]]
number = [1]
chrono = 3
links = ["https://en.wikipedia.org/wiki/Rocannon%27s_World"]
+++

A tale woven by a masterful storyteller with indulgently romantic prose.
The fantastical setting within the science fiction context is a fun one.

In the Hainish Cycle, we meet the League of All Worlds and see the inception of telepathy within it.
Interesting how psychic abilities were treated as possibility in late science fiction (Asimov’s Foundation series come to mind) unlike today.
We also are introduced to the War To Come and the idea that will become the Cultural Embargo.

I enjoy how while others cry FTL (Faster Than Light), Le Guin is more concerned with the social effects of NAFAL (Nearly As Fast As Light), as she will [later](@/posts/the-left-hand-of-darkness.md) coin it.
The extreme conditions of FTL mean it is possible for machines but people are stuck with NAFAL.
