+++
title = "Observation"
description = "“I’m sorry, Dave. I’m afraid I can’t do that,” I said."
date = 2020-11-06T09:27:22+00:00
updated = 2020-11-06T09:27:22+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["science fiction","horror","mystery","puzzle","thriller","fiction","speculative fiction"]
years = ["2019"]
artists = ["No Code"]
series = []
universe = []
[extra]
consumed = [["2020-05-22", "2020-05-27"]]
#number = 
links = ["https://en.wikipedia.org/wiki/Observation_(video_game)","https://www.observationgame.com/"]
+++

This game evokes strong feelings of the last two sequences of *2001: A Space Odyssey*, except you play as HAL 9000, or rather Sam (Systems Administration and Maintenance) in this case.
It’s interesting to play as a space station rather than a human.

I found the game captivating.
The setting of the space station is very cool, the atmosphere is dark and deeply unsettling, the mysteries are intriguing, and I became quite attached to Dr. Emma Fisher for a computer.
