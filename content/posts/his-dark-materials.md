+++
title = "His Dark Materials"
description = "The adventures of Lyra Silvertongue"
date = 2020-12-05T14:08:41+00:00
updated = 2021-05-03
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","fantasy", "speculative fiction","drama","adventure","bildungsroman","mystery","parallel universe"]
years = ["2019"]
artists = []
series = []
universe = ["Northern Lights"]
[extra]
consumed = [["2020-11-03",""]]
#number = 
chrono = 0.9
links = ["https://en.wikipedia.org/wiki/His_Dark_Materials_(TV_series)"]
+++

A worthy cinematisation of the nostalgic novels.

{{ season(number=2, link="https://en.wikipedia.org/wiki/His_Dark_Materials_(TV_series)#Series_2_(2020)", started="2020-11-08", finished="2021-01-05") }}
