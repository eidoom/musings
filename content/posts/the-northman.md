+++
title = "The Northman"
description = "(H)amleth"
date = 2022-05-13
updated = 2022-05-13
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["tragedy","epic","historical","fiction"]
years = ["2022"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-05-13
consumed = [["2022-05-13"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Northman"]
+++

Truly epic cinema.
A retelling of the tale of Hamlet, I mean, [Amleth](https://en.wikipedia.org/wiki/Amleth).
Could have done more interesting things with the story, though.
