+++
title = "The Long Way to a Small, Angry Planet"
description = ""
date = 2022-04-30
updated = 2022-05-11
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2014"]
artists = ["Becky Chambers"]
series = ["Wayfarers"]
universe = ["Galactic Commons"]
[extra]
active = false
published = 2022-04-30
consumed = [["2022-05-05","2022-05-11"]]
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/The_Long_Way_to_a_Small,_Angry_Planet"]
+++

A drama of the crew of the *Wayfarer*, the newest member Rosemary fitting in, and their biggest job yet.
