+++
title = "Season of Storms"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2013","2018"]
artists = ["Andrzej Sapkowski", "David French"]
series = []
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
#number = [0.6]
chrono = 1.5
links = ["https://en.wikipedia.org/wiki/Season_of_Storms"]
+++
