+++
title = "Cashback"
description = "The secret life of supermarket workers"
date = 2020-10-08T12:38:39+01:00
updated = 2020-10-08T12:38:39+01:00
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","drama","romance","fiction","england"]
years = ["2006"]
[extra]
consumed = [["2020-09-27"]]
+++

This is a film about an insomniac, but quite different from Fight Club.
Ben, an art student, works nights in Sainsbury’s.
He shares experiences which are either fantasy or the delusions of his sleep-deprived mind; it doesn’t matter which.
They are punctuated by creative shots, insights into working class subculture, and an obsession with the beauty of the female form.
It’s very funny, in a distinctly naughties British fashion.
