+++
title = "A Slower Speed of Light"
description = "Special relativity illuminated"
date = 2021-02-27T17:27:39+00:00
updated = 2021-02-27T17:27:39+00:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["libre","open source","free","special relativity","educational","science","physics","relativity"]
years = ["2012"]
artists = []
series = []
universe = []
[extra]
completed = true
consumed = [["2021-02-27"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/A_Slower_Speed_of_Light","http://gamelab.mit.edu/games/a-slower-speed-of-light/"]
+++

This is a free game with an educationally illustrative focus.
It is short, at most ten minutes (is that world time or player time?).
It has a touching little story and accompanying music for flavour.
By slowing the speed of light, the player is able to observe special relativistic effects of {{ wikipedia(name="Doppler shifting", slug="Relativistic_Doppler_effect") }} in the electromagnetic spectrum, {{ wikipedia(name="relativistic aberration") }}, {{ wikipedia(name="length contraction") }}, and {{ wikipedia(name="time dilation") }}.
It’s great!
