+++
title = "The Burning God"
description = ""
date = 2022-06-28
updated = 2022-06-28
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["grimdark","epic fantasy","fantasy","high fantasy"]
years = ["2020"]
artists = ["R. F. Kuang"]
series = ["The Poppy War Trilogy"]
universe = []
[extra]
active = false
published = 2022-06-28
consumed = [["2022-07-10","2022-07-24"]]
number = [3]
#chrono =
links = []
+++

