+++
title = "The Hope of Elantris"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["short story"]
tags = []
years = ["2006"]
artists = ["Brandon Sanderson"]
series = ["Elantris"]
universe = ["Cosmere"]
[extra]
published = 2021-07-30
consumed = []
number = [1.5]
chrono = 0
links = ["https://en.wikipedia.org/wiki/Elantris#The_Hope_of_Elantris"]
+++
