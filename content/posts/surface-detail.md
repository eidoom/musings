+++
title = "Surface Detail"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2010"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = [["2019-07-22", "2019-08-21"]]
number = [9]
chrono = 9
links = ["https://en.wikipedia.org/wiki/Surface_Detail"]
+++
