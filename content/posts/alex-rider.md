+++
title = "Alex Rider"
description = ""
date = 2020-12-05T14:15:39+00:00
updated = 2020-12-05T14:15:39+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["espionage","thriller","action","adventure","mystery","bildungsroman","england"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
published = 2021-09-05
consumed = [["2020-06-04",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Alex_Rider_(TV_series)"]
+++

