+++
title = "Elantris"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2005"]
artists = ["Brandon Sanderson"]
series = ["Elantris"]
universe = ["Cosmere"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 0
links = ["https://en.wikipedia.org/wiki/Elantris"]
+++
