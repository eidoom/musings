+++
title = "The Last Ringbearer"
description = ""
date = 2019-06-15
updated = 2022-04-24
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1999"]
artists = ["Kirill Yeskov", "Yisroel Markov"]
series = []
universe = ["Middle-Earth"]
[extra]
published = 2021-07-30
consumed = []
#number = 
chrono = 31.1
links = ["https://en.wikipedia.org/wiki/The_Last_Ringbearer"]
+++
