+++
title = "The Other Wind"
description = ""
date = 2022-04-19
updated = 2022-05-02
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2001"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = ["Earthsea"]
[extra]
active = false
published = 2022-04-19
consumed = [["2022-05-02"]]
number = [6]
chrono = 8
links = ["https://en.wikipedia.org/wiki/The_Other_Wind"]
+++

