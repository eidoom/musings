+++
title = "Doctor Strange in the Multiverse of Madness"
description = ""
date = 2022-06-29
updated = 2022-06-29
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["superhero"]
years = ["2022"]
artists = []
series = ["Doctor Strange"]
universe = ["Marvel Cinematic Universe"]
[extra]
active = false
published = 2022-06-29
consumed = [["2022-06-28"]]
number = [2]
chrono =28
links = ["https://en.wikipedia.org/wiki/Doctor_Strange_in_the_Multiverse_of_Madness"]
+++

