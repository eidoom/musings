+++
title = "Trine 2"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2011"]
artists = []
series = ["Trine"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-04-17",""]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Trine_2"]
+++

