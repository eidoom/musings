+++
title = "Mass Effect 2"
description = ""
date = 2022-05-07
updated = 2022-05-07
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2010"]
artists = []
series = []
universe = ["Mass Effect"]
[extra]
active = false
published = 2022-05-07
consumed = []
#number = []
chrono = 2
links = ["https://en.wikipedia.org/wiki/Mass_Effect_2"]
+++

