+++
title = "Tenet"
description = "It’s backwards day"
date = 2020-12-06T14:17:58+00:00
updated = 2020-12-06T14:17:58+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["action","speculative fiction","science fiction","adventure","mystery","thriller","espionage","time travel"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
consumed = [["2020-12-06"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Tenet_(film)"]
+++

This film contains incredibly cinematic scenes showing two flows of time—one backwards, one forwards—for the two subsets of the characters.
It’s not only epic but also deeply satisfying to see the confused motion of inverted people in one scene resolve into sensibility when we experience the scene in reverse. From the inverted perspective, the original motion becomes the chaotic one.
These flips are everywhere: the {{ wikipedia(name="title", slug="Sator_Square") }} is a palindrome and even the music employs {{ wikipedia(name="retrograde composition", slug="Retrograde_(music)") }}).

It's great to see {{ wikipedia(name="antiparticles", slug="Antimatter") }}, {{ wikipedia(name="relativistic causality", slug="Relativity_of_simultaneity") }}, and {{ wikipedia(name="arrow-of-time entropy", slug="Entropy_(arrow_of_time)") }} providing artistic inspiration.

## Dramatis personae 

|                        |                                                                                                                                              |
| ---------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| The Protagonist        | CIA agent enlisted in Tenet, a secret organisation aiming to stop the reversal of time/entropy (“inversion”)                                 |
| Neil                   | Tenet operative, works with The Protagonist                                                                                                  |
| Katherine “Kat” Barton | Estranged wife of Sator                                                                                                                      |
| Andrei Sator           | Russian oligarch, trades with the future and controls inversion technology in the present, wants to trigger universal inversion on his death |
| Priya Singh            | Indian arms dealer, Tenet member                                                                                                             |
| Volkov                 | Sator’s bodyguard                                                                                                                            |
| Ives                   | Tenet military commander                                                                                                                     |
| Mahir                  | Tenet fixer                                                                                                                                  |
| Wheeler                | Tenet Blue Team leader                                                                                                                       |
| Barbara                | Tenet scientist                                                                                                                              |
