+++
title = "Westworld"
description = ""
date = 2020-12-05T14:55:54+00:00
updated = 2020-12-05T14:55:54+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["android","artificial intelligence","speculative fiction","fiction","science fiction","drama","western","dystopian","mystery","action","adventure","nonlinear"]
years = ["2016"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2021-09-05
consumed = [["2017-10-01",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Westworld_(TV_series)"]
+++

{{ season(number=4, started="2022-07-10", finished="2022-08-25", year=2022, link="https://en.m.wikipedia.org/wiki/Westworld_(season_4)") }}
