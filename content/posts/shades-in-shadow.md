+++
title = "Shades in Shadow"
description = ""
date = 2020-08-02
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["collection","short story","triptych"]
tags = ["fantasy"]
years = ["2015"]
artists = ["N. K. Jemisin"]
series = []
universe = ["Inheritance"]
[extra]
published = 2021-07-30
consumed = [["2020-08-20", "2020-08-22"]]
#number = [0.5, 1.5, 2.5]
chrono = 0.5
links = []
+++
