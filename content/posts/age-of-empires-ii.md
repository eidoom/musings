+++
title = "Age of Empires II"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["1999","2013"]
artists = []
series = ["Age of Empires"]
universe = []
[extra]
alt = "Age of Empires II: HD Edition"
active = false
published = 2022-04-19
consumed = [["2018-11-22",""]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Age_of_Empires_II#HD_Edition"]
+++

Including the HD edition expansions:
* *The Forgotten*
* *The African Kingdoms*
* *Rise of the Rajas*
