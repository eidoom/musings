+++
title = "Equal Rites"
description = "Uproariously witty"
date = 2021-07-09T13:17:22+01:00
updated = 2021-07-25
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy","comedy","fiction","speculative fiction","magic"]
years = ["1987"]
artists = ["Terry Pratchett"]
series = ["Witches (Discworld)"]
universe = ["Discworld"]
[extra]
published = 2021-07-25
consumed = [["2021-07-22","2021-07-25"]]
number = [1]
chrono = 3
links = ["https://en.wikipedia.org/wiki/Equal_Rites"]
+++

Esk is given a wizard’s staff as a baby after she is mistaken for a son.
Granny Weatherwax, witch, tries to help her get admitted to the Unseen University to become the Discworld’s first female wizard.
