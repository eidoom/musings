+++
title = "Blade Runner 2049"
description = ""
date = 2021-10-25
updated = 2021-10-25
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2017"]
artists = []
series = ["Blade Runner"]
universe = []
[extra]
active = false
published = 2021-10-25
consumed = []
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Blade_Runner_2049"]
+++

