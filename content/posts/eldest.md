+++
title = "Eldest"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2005"]
artists = ["Christopher Paolini"]
series = ["The Inheritance Cycle"]
universe = []
[extra]
published = 2021-07-30
consumed = []
number = [2]
#chrono =
links = []
+++
