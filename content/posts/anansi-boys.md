+++
title = "Anansi Boys"
description = "The story of Fat Charlie"
date = 2020-10-12T09:42:03+01:00
updated = 2020-10-12T09:42:03+01:00
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy","speculative fiction","fiction","united states","england","comedy","united kingdom","america","britain"]
years = ["2005"]
artists = ["Neil Gaiman"]
universe = ["American Gods"]
[extra]
consumed = [["2020-10-07", "2020-10-12"]]
chrono = 2
links = ["https://en.wikipedia.org/wiki/Anansi_Boys"]
+++

Reading [The City We Became](../the-city-we-became), I was reminded of American Gods, which I read some time ago.
This decided my next read as Anansi Boys.

This book is hilarious. 
It follows Charlie Nancy, who discovers that he has a brother who is a god.
We have run-ins with Charlie’s bankster boss, the coven of elderly ladies from Charlie’s childhood street, Charlie’s fiancé’s formidable mother, and the atavistic old gods of prehistory.
Charlie’s clashes with his brother, or rather himself, help him get over his fear of singing in public and some other shackles besides.

Charlie’s experience of working for an investment firm in London reminded me of [Cashback](../cashback).

Gaiman mixes the absurd and oddly pragmatic to create a magical reality, where the magic makes perfect sense until you think about it too much. 
It wouldn’t be divine, otherwise.
