+++
title = "Zack Snyder’s Justice League"
description = "Comic books on the silver screen"
date = 2021-03-26T15:25:05+00:00
updated = 2021-05-07
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["superhero","fiction","speculative fiction","united states","fantasy","america"]
years = ["2021"]
artists = []
series = []
universe = ["DC Extended Universe"]
[extra]
consumed = [["2021-03-25"]]
#number = 
chrono = 10
links = ["https://en.wikipedia.org/wiki/Zack_Snyder%27s_Justice_League"]
+++

Utterly ridiculous.
Not unenjoyable though.
