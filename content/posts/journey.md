+++
title = "Journey"
description = ""
date = 2021-04-22T08:04:24+01:00
updated = 2021-04-22T08:04:24+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["arthouse","adventure"]
years = ["2012","2019"]
artists = ["Thatgamecompany","Santa Monica Studio","Austin Wintory"]
series = []
universe = []
[extra]
published = 2022-05-07
consumed = [["2021-04-18",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Journey_(2012_video_game)"]
+++

