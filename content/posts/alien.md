+++
title = "Alien"
description = "A film for cat lovers"
date = 2021-01-17T22:35:06+00:00
updated = 2021-01-23
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["science fiction","horror","speculative fiction","fiction","aliens","thriller","mystery"]
years = ["1979"]
artists = ["Ridley Scott"]
series = ["Alien original series"]
universe = ["Alien"]
[extra]
consumed = [["2021-01-17"]]
number = [1]
chrono = 3
links = ["https://en.wikipedia.org/wiki/Alien_(film)"]
+++

