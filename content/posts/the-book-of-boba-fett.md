+++
title = "The Book of Boba Fett"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = []
years = ["2021"]
artists = []
series = []
universe = ["Star Wars"]
[extra]
active = false
published = 2022-04-19
consumed = [["2022-01-12",""]]
#number = []
chrono = 6.6
links = ["https://en.wikipedia.org/wiki/The_Book_of_Boba_Fett"]
+++

