+++
title = "Eclipse: Second Dawn for the Galaxy"
description = "Settlers of the GCDS"
date = 2021-06-15T10:20:34+01:00
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = ["4x","space opera","space","aliens","science fiction","speculative fiction","fiction","far future","amerigame","wargame","space exploration","civilisation game"]
years = ["2020"]
artists = []
series = []
universe = []
[extra]
players = [1,6]
consumed = [["2021-06-15",],["2021-06-18",],]
#number = 
#chrono =
links = ["https://www.boardgamegeek.com/boardgame/246900/eclipse-second-dawn-galaxy","https://en.lautapelit.fi/product/24681/eclipse—2nd-dawn-for-the-galaxy-eng"]
+++

This 2020 release is a revised version of the original 2011 edition, *Eclipse: New Dawn for the Galaxy*.

It’s like they made [Stellaris](@/posts/stellaris.md) into a board game.
Well, except the classic version predates Stellaris so it’s really the other way around.

Very fun to play.
Suberp artwork and gameplay mechanics.

I played with four players.
