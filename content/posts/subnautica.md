+++
title = "Subnautica"
description = "Thalassophobia: the game"
date = 2020-10-08T11:11:15+01:00
updated = 2020-10-08T11:11:15+01:00
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["exploration","story","crafting","survival","open world","fiction","science fiction","speculative fiction"]
years = ["2018"]
artists = ["Unknown Worlds Entertainment"]
series = ["Subnautica"]
universe = []
[extra]
completed = true
consumed = [["2019-02-11", ""]]
number = [1]
links = ["https://en.wikipedia.org/wiki/Subnautica","https://unknownworlds.com/subnautica/"]
+++

This game is beautiful. 
I love swimming around the various biomes and studying the alien fauna.
Uncovering the secrets of this world was captivating.
