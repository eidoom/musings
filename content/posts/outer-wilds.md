+++
title = "Outer Wilds"
description = "A beautifully crafted story"
date = 2021-01-06T12:29:25+00:00
updated = 2021-01-23
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["temporal loop","science fiction","fiction","speculative fiction","aliens","exploration","mystery","story"]
years = ["2019"]
artists = ["Mobius Digital"]
series = ["Outer Wilds"]
universe = []
[extra]
consumed = [["2021-01-10","2021-01-22"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Outer_Wilds","https://www.mobiusdigitalgames.com/outer-wilds.html","https://outerwilds.gamepedia.com/Outer_Wilds_Wiki"]
+++

Wow, what a way to tell a story!
A whole solar system full of dynamic behaviour to marvel at, ancient mysteries to unravel, and wholesome friends to greet.
