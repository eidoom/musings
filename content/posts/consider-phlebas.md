+++
title = "Consider Phlebas"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1987"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Consider_Phlebas"]
+++
