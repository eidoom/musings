+++
title = "The State of the Art"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["collection","short story","novella"]
tags = ["science fiction"]
years = ["1991"]
artists = ["Iain M. Banks"]
series = ["Culture"]
universe = ["The Culture"]
[extra]
published = 2021-07-30
consumed = []
number = [4]
chrono = 3
links = []
+++
