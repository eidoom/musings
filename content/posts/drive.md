+++
title = "Drive"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["short story"]
tags = ["science fiction"]
years = ["2012"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [2.6]
chrono = 2.6
links = []
+++
