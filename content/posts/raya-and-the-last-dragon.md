+++
title = "Raya and the Last Dragon"
description = "An Asian fantasy"
date = 2021-03-12T23:28:37+00:00
updated = 2021-03-12T23:28:37+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["animated","fiction","fantasy", "speculative fiction","family friendly","dragon"]
years = ["2021"]
artists = ["Walt Disney Animation Studios"]
series = []
universe = []
[extra]
consumed = [["2021-03-12"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Raya_and_the_Last_Dragon"]
+++

Damn, that’s a good render.
