+++
title = "FernGully: The Last Rainforest"
description = ""
date = 2022-06-26
updated = 2022-06-26
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["animated","musical","fantasy"]
years = ["1992"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-06-26
consumed = [["2022-06-26"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/FernGully:_The_Last_Rainforest"]
+++

