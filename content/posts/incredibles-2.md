+++
title = "Incredibles 2"
description = "Super nostalgia"
date = 2021-06-28T09:53:51+01:00
updated = 2021-06-28T09:53:51+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["family friendly","animated","superhero"]
years = ["2018"]
artists = ["Pixar Animation Studios"]
series = []
universe = []
[extra]
published = 2021-06-28
consumed = [["2021-06-27"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Incredibles_2"]
+++

