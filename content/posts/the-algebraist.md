+++
title = "The Algebraist"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2004"]
artists = ["Iain M. Banks"]
series = []
universe = []
[extra]
published = 2021-07-30
consumed = [["2019-12-31", "2020-01-23"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Algebraist"]
+++
