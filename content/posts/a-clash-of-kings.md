+++
title = "A Clash of Kings "
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1998"]
artists = ["George R. R. Martin"]
series = ["A Song of Ice and Fire"]
universe = ["Game of Thrones"]
[extra]
published = 2021-07-30
consumed = []
number = [2]
chrono = 2
links = []
+++
