+++
title = "Life is Strange"
description = "...and beautiful"
date = 2020-11-22T22:09:38+00:00
updated = 2021-10-04
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["adventure","time travel","fantasy", "speculative fiction","fiction","episodic","school","teen","story","drama","united states","pacific northwest","america"]
years = ["2015"]
artists = ["Dontnod Entertainment"]
series = ["Life is Strange"]
universe = []
[extra]
consumed = [["2020-07-08", "2020-07-15"]]
number = [1]
links = ["https://en.wikipedia.org/wiki/Life_Is_Strange","https://lifeisstrange.square-enix-games.com/en-gb/games/life-is-strange"]
+++

This game tells an engaging and beautiful story about the friendship between two young adults, our protagonist Max Caulfield and Chloe Price.
The narrative is charged with drama and emotion, tension and surprise, darkness and a sublime nostalgic feeling of the final school year to first year of undergraduate time period (my Scottish translation of America’s college).
The seaside small-town setting of Arcadia Bay is tranquil and familiar, the music, original and borrowed, blends wonderfully, and the stylistic painted-art aesthetic of the graphics creates a dreamy canvas for our characters.

The game takes the common narrative-driven cheat of save scumming and turns it into the core mechanic of the game: Max can turn back time.
This is possibly the only use of time travel I’ve ever been satisfied with in a yarn.
