+++
title = "Free Guy"
description = ""
date = 2021-10-01
updated = 2021-10-01
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","romance","action","science fiction"]
years = ["2021"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2021-10-01
consumed = [["2021-09-30"]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Free_Guy"]
+++

