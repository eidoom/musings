+++
title = "Mass Effect"
description = ""
date = 2022-05-07
updated = 2022-05-07
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2007"]
artists = []
series = []
universe = ["Mass Effect"]
[extra]
active = false
published = 2022-05-07
consumed = []
#number = []
chrono = 1
links = ["https://en.wikipedia.org/wiki/Mass_Effect_(video_game)"]
+++

