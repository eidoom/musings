+++
title = "Death on the Nile"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2022"]
artists = []
series = ["Kenneth Branagh’s Poirot"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-03-28"]]
number = [2]
#chrono =
links = ["https://en.wikipedia.org/wiki/Death_on_the_Nile_(2022_film)"]
+++

