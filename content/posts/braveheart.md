+++
title = "Braveheart"
description = "Aye"
date = 2021-07-07T22:25:40+01:00
updated = 2021-07-07T22:25:40+01:00
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["epic","heroic","historic","war","scotland","fiction","britain","england"]
years = ["1995"]
artists = []
series = []
universe = []
[extra]
published = 2021-07-07
consumed = [["2021-07-07"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Braveheart"]
+++

