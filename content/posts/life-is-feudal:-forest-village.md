+++
title = "Life is Feudal: Forest Village"
description = "Banished clone"
date = 2021-06-08T19:06:14+01:00
updated = 2021-06-08T19:06:14+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["city builder","medieval","survival","simulation","indie","early access","strategy"]
years = ["2016","2017"]
artists = ["Mindillusion"]
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-04-30",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Life_Is_Feudal"]
+++

{{ playthrough(name="Initial impressions",started="2021-05-04",finished="2021-05-04") }}

A bit clunky but could be fun when in the mood.
