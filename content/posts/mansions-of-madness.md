+++
title = "Mansions of Madness"
description = ""
date = 2022-04-19
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game","role-playing game"]
tags = ["tabletop","strategy","lovecraft","horror","coop","cooperative","adventure","mystery"]
years = ["2011","2016"]
artists = []
series = []
universe = []
[extra]
alt = "Mansions of Madness Second Edition"
active = false
published = 2022-04-19
consumed = [["2022-04-14",""]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Mansions_of_Madness","https://boardgamegeek.com/boardgame/205059/mansions-madness-second-edition"]
+++

I played with five players.
