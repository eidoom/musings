+++
title = "Catan"
description = ""
date = 2022-05-06
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = []
years = ["1995"]
artists = []
series = []
universe = []
[extra]
alt = "The Settlers of Catan"
active = false
#published = 2022-05-06
consumed = []
#number = []
#chrono =
links = ["https://boardgamegeek.com/boardgame/13/catan"]
+++

