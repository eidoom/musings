+++
title = "Dixit"
description = ""
date = 2022-08-23
updated = 2022-08-23
draft = false
[taxonomies]
media = ["game"]
formats = ["card game"]
tags = []
years = ["2008"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-08-23
consumed = []
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Dixit_(card_game)","https://www.libellud.com/en/our-games/dixit/","https://boardgamegeek.com/boardgame/39856/dixit"]
+++

