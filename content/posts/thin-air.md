+++
title = "Thin Air"
description = ""
date = 2020-03-01
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2018"]
artists = ["Richard K. Morgan"]
series = []
universe = ["Genetically modified"]
[extra]
published = 2021-07-30
consumed = [["2020-04-13", "2020-04-24"]]
#number = 
chrono = 2
links = ["https://en.wikipedia.org/wiki/Thin_Air_(Morgan_novel)"]
+++
