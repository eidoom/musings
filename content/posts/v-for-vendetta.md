+++
title = "V for Vendetta"
description = "Orwellian dystopia behind a Guy Fawkes mask"
date = 2021-05-24T22:05:42+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["speculative fiction","britain","london","england","united kingdom","political","action","dystopian"]
years = ["2005"]
artists = []
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-05-24"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/V_for_Vendetta_(film)"]
+++

This film was a bit all over the place, recognisably Wachowski.
Quite fun though, with some very enjoyable scenes.
