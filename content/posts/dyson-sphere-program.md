+++
title = "Dyson Sphere Program"
description = "Another 3d Factorio"
date = 2021-02-02T12:48:00+00:00
updated = 2021-02-07
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["simulation","factory"]
years = ["2021"]
artists = []
series = []
universe = []
[extra]
completed = false
consumed = [["2021-02-01",""]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Dyson_Sphere_Program"]
+++

I love the premise, but I’ve too much Factorio fatigue to commit to it just now.
