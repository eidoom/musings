+++
title = "Altered Carbon"
description = ""
date = 2020-12-05T14:29:28+00:00
updated = 2020-12-05T14:29:28+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","speculative fiction","science fiction","cyberpunk","mystery","action","adventure","dystopian","social divide"]
years = ["2018"]
artists = []
series = []
universe = ["Altered Carbon"]
[extra]
published = 2021-09-05
consumed = [["2018-02-02","2020-02-29"]]
#number = 
chrono = 1.1
links = ["https://en.wikipedia.org/wiki/Altered_Carbon_(TV_series)"]
+++

{{ season(number=1) }}

{{ season(number=2,started="2020-02-27",finished="2020-02-29") }}
