+++
title = "Climate Change: The Facts"
description = "“The future looks alarming indeed”"
date = 2021-02-18T08:48:50+00:00
updated = 2021-02-18T08:48:50+00:00
draft = false
[taxonomies]
media = ["video"]
formats = ["television","documentary"]
tags = ["environment","ecology","factual"]
years = ["2019"]
artists = ["David Attenborough"]
series = []
universe = []
[extra]
consumed = [["2021-02-18"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Climate_Change_%E2%80%93_The_Facts"]
+++

It’s good to have a recommendation like this documentary in the pocket to suggest to just about everyone.
