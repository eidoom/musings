+++
title = "Auberon"
description = ""
date = 2019-11-25
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novella"]
tags = []
years = ["2019"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [8.5]
chrono = 8.5
links = ["https://en.wikipedia.org/wiki/Persepolis_Rising#%22Auberon%22"]
+++
