+++
title = "Catch Me If You Can"
description = "Con-boy"
date = 2021-05-24T12:28:51+01:00
updated = 2021-06-08
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["biography","drama","crime"]
years = ["2002"]
artists = []
series = []
universe = []
[extra]
published = 2021-06-08
consumed = [["2021-05-23"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Catch_Me_If_You_Can"]
+++

Painting the confidence artist so sympathetically and impressively is exactly how they’d want you to portray them...

In any case, I fell for it entirely.
