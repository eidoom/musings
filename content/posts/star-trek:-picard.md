+++
title = "Star Trek: Picard"
description = "Jean-Luc Picard has some fun in his sunset years"
date = 2020-12-05
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","speculative fiction","science fiction","action","adventure","drama","space","future","aliens","artificial intelligence","android"]
years = ["2020"]
artists = []
series = []
universe = ["Star Trek"]
[extra]
active = false
consumed = [["2020-01-23",""]]
#number = 
chrono = 9
links = ["https://en.wikipedia.org/wiki/Star_Trek:_Picard"]
+++

{{ season(number=1, started="2020-01-23", finished="2020-03-26",link="https://en.wikipedia.org/wiki/Star_Trek:_Picard_(season_1)") }}

With his own days numbered, Jean-Luc is desperate to ensure a future for android life, in particular one who may be Data’s daughter.
Having burnt his bridges with Starfleet in protest of their behaviour, he has to appeal to old friends for help.

{{ season(number=2, started="2022-03-13", finished="2022-05-05", link="https://en.wikipedia.org/wiki/Star_Trek:_Picard_(season_2)") }}

Now it's Q's turn to face his demise and he's taking Picard along for the ride.
We go via the Terran Empire to a pivotal moment in Earth's history that seeds the Confederation, and birth a new Borg.
Picard must come to terms with a [cornerstone](@/posts/westworld.md) childhood trauma to satisfy Q.

I noticed with much amusement that the spaceship rumble plays almost entirely throughout the season, in particular when we're not on a spaceship.
