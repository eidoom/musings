+++
title = "Star Trek: The Next Generation"
description = ""
date = 2021-10-10
updated = 2021-10-10
draft = false
[taxonomies]
media = ["video"]
formats = ["television"]
tags = ["fiction","speculative fiction","science fiction","space opera"]
years = ["1987"]
artists = []
series = []
universe = ["Star Trek"]
[extra]
active = false
published = 2021-10-10
consumed = []
#number = []
chrono = 8
links = ["https://en.wikipedia.org/wiki/Star_Trek:_The_Next_Generation"]
+++

There is *a lot* to watch: 178 episodes, each at 45 minutes!
I saw many of them as a kid on the TV, leaving only a nostalgic sense of the setting and characters.
Now I’ll pick and choose some as recommended by friends and the [Let’s Watch Star Trek](https://www.letswatchstartrek.com/tng-episode-guide/) blog.

{{ episode(season=1,episodes=[1,2],title="Encounter at Farpoint",links=["https://en.wikipedia.org/wiki/Encounter_at_Farpoint"]) }}

{{ episode(season=1,episode=25,title="Conspiracy",seen="2021-10-10",links=["https://en.wikipedia.org/wiki/Conspiracy_(Star_Trek:_The_Next_Generation)"]) }}

{{ episode(season=2,episode=16,title="Q Who",seen="2021-10-08") }}

{{ episode(season=3,episode=4,title="Who Watches The Watchers",seen="2021-10-07") }}
