+++
title = "Half-Life"
description = ""
date = 2022-04-21
updated = 2022-04-21
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["1998"]
artists = []
series = ["Half-Life"]
universe = ["Half-Life"]
[extra]
active = false
published = 2022-04-21
consumed = []
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Half-Life_(video_game)"]
+++
Including:
* Half-Life: Opposing Force
* Half-Life: Blue Shift

Remade in [Black Mesa](@/posts/black-mesa.md).
