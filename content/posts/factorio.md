+++
title = "Factorio"
description = "Automate automation"
date = 2020-11-22T22:46:47+00:00
updated = 2020-11-25
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = ["open world","construction and management sim","indie","early access","real-time strategy","strategy","base builder","aliens","science fiction","2d","speculative fiction","rts","real-time"]
years = ["2016","2020"]
artists = ["Wube Software"]
series = []
universe = []
[extra]
completed = true
consumed = [["2018-06-12", ""]] 
#number = 
links = ["https://en.wikipedia.org/wiki/Factorio","https://www.factorio.com/"]
+++

The nirvana of satisfaction.
