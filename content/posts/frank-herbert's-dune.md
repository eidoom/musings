+++
title = "Frank Herbert’s Dune"
description = ""
date = 2021-09-05
updated = 2021-09-05
draft = false
[taxonomies]
media = ["video"]
formats = ["television","miniseries"]
tags = ["science fiction"]
years = ["2000"]
artists = ["John Harrison"]
series = []
universe = ["Dune"]
[extra]
active = false
published = 2021-09-05
consumed = [] # 2020
#number = []
chrono = 1.2
links = ["https://en.wikipedia.org/wiki/Frank_Herbert's_Dune"]
+++

