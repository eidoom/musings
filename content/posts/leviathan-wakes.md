+++
title = "Leviathan Wakes"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2011"]
artists = ["James S. A. Corey"]
series = ["The Expanse"]
universe = ["The Expanse"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Leviathan_Wakes"]
+++
