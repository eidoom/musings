+++
title = "Trine: Enchanted Edition"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["game"]
formats = ["computer game"]
tags = []
years = ["2009","2014"]
artists = ["Frozenbyte"]
series = ["Trine"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-04-15",""]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Trine_(video_game)"]
+++

