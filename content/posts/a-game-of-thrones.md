+++
title = "A Game of Thrones"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1996"]
artists = ["George R. R. Martin"]
series = ["A Song of Ice and Fire"]
universe = ["Game of Thrones"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 1
links = []
+++
