+++
title = "Sword of Destiny"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1992","2015"]
artists = ["Andrzej Sapkowski","David French"]
series = []
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
#number = [2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/Sword_of_Destiny"]
+++
