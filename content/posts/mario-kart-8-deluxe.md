+++
title = "Mario Kart 8 Deluxe"
description = ""
date = 2022-08-18
updated = 2022-08-18
draft = false
[taxonomies]
media = ["game"]
formats = ["video game"]
tags = ["nintendo switch"]
years = ["2017"]
artists = ["Nintendo EAD"]
series = []
universe = []
[extra]
active = false
published = 2022-08-18
consumed = [["2022-08-17",""]]
#number = []
#chrono =
links = ["https://en.wikipedia.org/wiki/Mario_Kart_8#Mario_Kart_8_Deluxe"]
+++

