+++
title = "City of Illusions"
description = "I am lying"
date = 2021-06-05T22:44:23+01:00
updated = 2021-06-27
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction","science fiction","fiction","far future","fantasy","amnesia"]
years = ["1967"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
published = 2021-06-08
consumed = [["2021-06-05","2021-06-08"]]
number = [3]
chrono = 5
links = ["https://en.wikipedia.org/wiki/City_of_Illusions"]
+++

A longer novel this time, although still ending suddenly.
A bitter fate for the future of an alternative Earth.

We share our protagonist’s struggle with self-identity and truth as each new encounter casts the last in doubt.
The varied societies across the planet’s face bear different aspects of the human experience.

In the cycle, we see the aftermath of the success of the Enemy (Shing) and the start of their overthrowal by the Alterrans (Werelians), leading to the establishment of the Ecumen.
