+++
title = "Star Wars: Episode VII – The Force Awakens"
description = ""
date = 2020-11-29T16:56:38+00:00
updated = 2020-11-29T16:56:38+00:00
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["epic","science fiction","fantasy", "speculative fiction","space opera","space","fiction"]
years = ["2015"]
artists = []
series = ["Skywalker saga","Star Wars sequel trilogy"]
universe = ["Star Wars"]
[extra]
consumed = [["2020-11-28"]]
number = [7,1]
chrono = 7
links = ["https://en.wikipedia.org/wiki/Star_Wars:_The_Force_Awakens"]
+++

