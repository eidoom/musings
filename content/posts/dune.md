+++
title = "Dune"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1965"]
artists = ["Frank Herbert"]
series = ["Dune Chronicles","Gollancz 50"]
universe = ["Dune"]
[extra]
published = 2021-07-30
consumed = []
number = [1,5]
chrono = 1
links = ["https://en.wikipedia.org/wiki/Dune_(novel)"]
+++
