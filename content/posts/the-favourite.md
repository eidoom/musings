+++
title = "The Favourite"
description = "Striking mise-en-scène, amusing, doleful"
date = 2021-07-03T22:38:58+01:00
updated = 2021-07-10
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["comedy","drama","historical"]
years = ["2018"]
artists = []
series = []
universe = []
[extra]
published = 2021-07-03
consumed = [["2021-07-03"]]
#number = 
#chrono =
links = []
+++

