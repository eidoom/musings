+++
title = "Salvation Lost"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction"]
years = ["2019"]
artists = ["Peter F. Hamilton"]
series = ["Salvation Sequence"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2019-11-14", "2019-11-25"]]
number = [2]
#chrono =
links = []
+++
