+++
title = "The Killing Moon"
description = ""
date = 2020-08-20
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["fantasy"]
years = ["2012"]
artists = ["N.K. Jemisin"]
series = ["Dreamblood"]
universe = []
[extra]
published = 2021-07-30
consumed = [["2020-08-22", "2020-08-29"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/The_Killing_Moon_(novel)"]
+++
