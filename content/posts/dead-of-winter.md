+++
title = "Dead of Winter"
description = ""
date = 2022-05-06
updated = 2022-05-06
draft = false
[taxonomies]
media = ["game"]
formats = ["board game"]
tags = []
years = ["2014"]
artists = []
series = ["Crossroads"]
universe = []
[extra]
alt = "Dead of Winter: A Crossroads Game"
active = false
published = 2022-05-06
consumed = []
number = [1]
#chrono =
links = ["https://boardgamegeek.com/boardgame/150376/dead-winter-crossroads-game"]
+++

