+++
title = "God Emperor of Dune"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1981"]
artists = ["Frank Herbert"]
series = ["Dune Chronicles"]
universe = ["Dune"]
[extra]
published = 2021-07-30
consumed = []
number = [4]
chrono = 4
links = ["https://en.wikipedia.org/wiki/God_Emperor_of_Dune"]
+++
