+++
title = "Blood of Elves"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1994","2008"]
artists = ["Andrzej Sapkowski", "Danusia Stok"]
series = ["The Witcher Saga"]
universe = ["The Witcher"]
[extra]
published = 2021-07-30
consumed = []
number = [1]
chrono = 3
links = ["https://en.wikipedia.org/wiki/Blood_of_Elves"]
+++
