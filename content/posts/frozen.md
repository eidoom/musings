+++
title = "Frozen"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = []
years = ["2013"]
artists = ["Walt Disney Animation Studios"]
series = ["Frozen"]
universe = []
[extra]
active = false
published = 2022-04-19
consumed = [["2022-01-22"]]
number = [1]
#chrono =
links = ["https://en.wikipedia.org/wiki/Frozen_(2013_film)"]
+++

