+++
title = "The Hundred Thousand Kingdoms"
description = ""
date = 2020-08-02
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2010"]
artists = ["N. K. Jemisin"]
series = ["The Inheritance Trilogy"]
universe = ["Inheritance"]
[extra]
published = 2021-07-30
consumed = [["2020-07-25", "2020-08-04"]]
number = [1]
chrono = 1
links = ["https://en.wikipedia.org/wiki/The_Hundred_Thousand_Kingdoms"]
+++
