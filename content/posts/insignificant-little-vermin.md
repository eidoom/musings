+++
title = "Insignificant Little Vermin"
description = "Fun prototype"
date = 2021-07-08T17:32:27+01:00
updated = 2021-07-08T17:32:27+01:00
draft = false
[taxonomies]
media = ["game"]
formats = ["interactive fiction"]
tags = ["fantasy","sword and sorcery","high fantasy","text-based","rpg","procedural","adventure","fiction","speculative fiction"]
years = ["2017"]
artists = []
series = []
universe = []
[extra]
published = 2021-07-08
consumed = [["2021-07-08"]]
#number = 
#chrono =
links = ["https://egamebook.com/vermin/v/latest/","https://github.com/filiph/egamebook"]
+++

