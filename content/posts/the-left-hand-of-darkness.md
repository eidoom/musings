+++
title = "The Left Hand of Darkness"
description = "Bridging cultures, genders, nations"
date = 2021-06-08T17:40:16+01:00
updated = 2021-08-07
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction","science fiction","fiction","far future","fantasy"]
years = ["1969"]
artists = ["Ursula K. Le Guin"]
series = ["Hainish Cycle"]
universe = ["The Ekumen"]
[extra]
published = 2021-06-19
consumed = [["2021-06-08","2021-06-19"]]
number = [5]
chrono = 6
links = ["https://en.wikipedia.org/wiki/The_Left_Hand_of_Darkness"]
+++

> “...the perfect uselessness of knowing the answer to the wrong question.”

I cannot advise you to read this novel, lest waive *shifgrethor*, but I can say I enjoyed it very much.

The setting is the planet Gethen, called Winter in the Ekumen, reminiscent of [the first Werel](@/posts/planet-of-exile.md).
The glacial climate breeds a race of tough survivors.
The major states are: Karhide, a monarchy with a capital Erhenrang and countryside full of generous, welcoming denizens; and Orgoreyn, an authoritarian oligarchy, where you’d better have your papers in order and the Sarf (secret police) are always watching.
The Ekumen has recently made contact; Gethen has not yet joined.

The anthropoids of Gethen are biologically ambisexual: their ordinary state, *somer*, is asexual, but once a month they enter *kemmer*, when they become sexually male or female.

The story concerns the relationship between Genly Ai, Terran man and Ekumenical Envoy to Gethen, and Therem Harth rem ir Estraven, the King’s Ear of Karhide and later refugee in Orgoreyn.
They struggle to understand one another because of the great gulf between their cultures, but eventually connect through their differences.
