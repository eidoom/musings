+++
title = "The Broken Kingdoms"
description = ""
date = 2020-08-02
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2010"]
artists = ["N. K. Jemisin"]
series = ["The Inheritance Trilogy"]
universe = ["Inheritance"]
[extra]
published = 2021-07-30
consumed = [["2020-08-04", "2020-08-11"]]
number = [2]
chrono = 2
links = ["https://en.wikipedia.org/wiki/The_Broken_Kingdoms"]
+++
