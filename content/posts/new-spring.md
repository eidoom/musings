+++
title = "New Spring"
description = ""
date = 2019-06-15
updated = 2021-07-30
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["2004"]
artists = ["Robert Jordan"]
series = ["The Wheel of Time"]
universe = ["The Wheel of Time"]
[extra]
published = 2021-07-30
consumed = []
number = [0]
chrono = 0
links = ["https://en.wikipedia.org/wiki/New_Spring"]
+++
