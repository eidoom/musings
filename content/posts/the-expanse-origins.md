+++
title = "The Expanse Origins"
description = ""
date = 2022-05-05
updated = 2022-05-05
draft = false
[taxonomies]
media = ["book"]
formats = ["comic"]
tags = []
years = ["2017"]
artists = []
series = []
universe = []
[extra]
active = false
published = 2022-05-05
consumed = []
#number = []
#chrono =
links = ["https://expanse.fandom.com/wiki/The_Expanse_Origins"]
+++

