+++
title = "Dune: Part One"
description = ""
date = 2021-10-24
updated = 2021-10-24
draft = false
[taxonomies]
media = ["video"]
formats = ["film"]
tags = ["science fiction","fiction","speculative fiction"]
years = ["2021"]
artists = ["Denis Villeneuve"]
series = []
universe = ["Dune"]
[extra]
active = false
published = 2021-10-24
consumed = [["2021-10-23"]]
#number = []
chrono = 1.3
links = ["https://en.wikipedia.org/wiki/Dune_(2021_film)"]
+++

