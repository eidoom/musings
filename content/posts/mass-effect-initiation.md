+++
title = "Initiation"
description = "Harper joins the Initiative"
date = 2020-10-12T12:13:01+01:00
updated = 2020-10-12T12:13:01+01:00
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["science fiction","space opera","speculative fiction","fiction"]
years = ["2017"]
artists = ["N. K. Jemisin","Mac Walters"]
series = ["Mass Effect: Andromeda"]
universe = ["Mass Effect"]
[extra]
consumed = [["2020-09-22", "2020-09-25"]]
number = [2]
chrono = 3.4
links = ["https://en.wikipedia.org/wiki/Mass_Effect:_Andromeda_(book_series)#Initiation"]
+++

The story of how Cora Harper met Alec Ryder (not to be confused with Alex Rider) and joined the Andromeda Initiative.
Since I found Cora to be less interesting than other crew members in Mass Effect: Andromeda, I was surprised to find I enjoyed this book a lot.
It played out with all the best features of an old-school Mass Effect mission.
