+++
title = "The Farthest Shore"
description = ""
date = 2022-04-19
updated = 2022-04-19
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = []
years = ["1972"]
artists = ["Ursula K. Le Guin"]
series = ["The Books of Earthsea"]
universe = ["Earthsea"]
[extra]
active = false
published = 2022-04-19
consumed = [["2022-02-01","2022-02-17"]]
number = [3]
chrono = 5
links = ["https://en.wikipedia.org/wiki/The_Farthest_Shore"]
+++

