+++
title = "Extinction: The Facts"
description = "Ecological collapse is today’s problem"
date = 2020-10-01T18:12:16+01:00
updated = 2020-10-01T18:12:16+01:00
[taxonomies]
media = ["video"]
formats = ["television","documentary"]
tags = ["environment","life","nature","nonfiction","ecology","factual"]
years = ["2020"]
artists = ["David Attenborough"]
[extra]
consumed = [["2020-09-28"]]
links = ["https://en.wikipedia.org/wiki/Extinction:_The_Facts"]
+++

I watched David Attenborough’s [Extinction: The Facts](https://www.thetvdb.com/movies/extinction-the-facts).
This simply should be mandatory viewing for Britons.
