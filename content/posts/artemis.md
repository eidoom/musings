+++
title = "Artemis"
description = "Lunar capers"
date = 2021-05-03T21:27:48+01:00
updated = 2021-05-07
draft = false
[taxonomies]
media = ["book"]
formats = ["novel"]
tags = ["speculative fiction", "science fiction", "hard sci-fi", "near future"]
years = ["2017"]
artists = ["Andy Weir"]
series = []
universe = []
[extra]
consumed = [["2021-05-03","2021-05-07"]]
#number = 
#chrono =
links = ["https://en.wikipedia.org/wiki/Artemis_(novel)"]
+++

A quick read; fun hard sci-fi.
