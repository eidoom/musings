{%- set dateformat = "%e %b %Y" -%}

## {{ name }}

<div class="meta">
<div>
<span class="sub">
{%- if started and finished -%}
{%- if started == finished -%}
Played on {{ started | date(format=dateformat) }}
{%- else -%}
{%- set m1 = started | date(format="%m") | int -%}
{%- set m2 = finished | date(format="%m") | int -%}
{%- set y1 = started | date(format="%Y") | int -%}
{%- set y2 = finished | date(format="%Y") | int -%}
{%- if y1 == y2 -%}
    {%- if m1 == m2 -%}
        {%- set short = "%e" -%}
    {%- else -%}
        {%- set short = "%e %b" -%}
    {% endif %}
{%- else -%}
    {%- set short = dateformat -%}
{%- endif %}
Played from {{ started | date(format=short) }} to {{ finished | date(format=dateformat) }}
{%- endif -%}
{%- endif -%}
{%- if started and not finished -%}
Playing since {{ started | date(format=dateformat) }}
{%- endif -%}
</span>
</div>
</div>
