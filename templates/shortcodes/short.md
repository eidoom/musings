{%- set dateformat = "%e %b %Y" -%}

## {{ title }}

<div class="meta">
<div>
{%- if alt -%}
<span class=tag>{{- alt -}}</span>
{%- endif -%}
{%- if year -%}
<span class=tag>{{- year -}}</span>
{%- endif -%}
{%- if years -%}
{%- for year in years -%}
<span class=tag>{{- year -}}</span>
{%- endfor -%}
{%- endif -%}
{%- if link -%}
<span class=tag><a href="{{ link | safe }}">{{ link | split(pat="/") | nth(n=2) }}</a></span>
{%- endif -%}
{%- if links -%}
{%- for link in links -%}
<span class=tag><a href="{{ link | safe }}">{{ link | split(pat="/") | nth(n=2) }}</a></span>
{%- endfor -%}
{%- endif -%}
{%- if uni -%}
<span class=tag>
{{- uni -}}{%- if num -%}&nbsp# {{- num -}}{%- endif -%}
</span>
{%- if rest -%}
{%- for i in range(end=rest|length) -%}
{%- if loop.index is even -%}{%- continue -%}{%- endif -%}
<span class=tag>
{%- set j = i + 1 -%}
<a href="{{- rest[j] -}}">#{{- rest[i] -}}</a>
</span>
{%- endfor -%}
{%- endif -%}
{%- endif -%}
{%- if started and finished -%}
{%- set m1 = started | date(format="%m") | int -%}
{%- set m2 = finished | date(format="%m") | int -%}
{%- set y1 = started | date(format="%Y") | int -%}
{%- set y2 = finished | date(format="%Y") | int -%}
{%- if y1 == y2 -%}
    {%- if m1 == m2 -%}
        {%- set short = "%e" -%}
    {%- else -%}
        {%- set short = "%e %b" -%}
    {% endif %}
{%- else -%}
    {%- set short = dateformat -%}
{%- endif %}
<span class="sub">Read from {{ started | date(format=short) }} to {{ finished | date(format=dateformat) }} </span>
{%- endif -%}
{%- if started and not finished -%}
<span class="sub">Read {{ started | date(format=dateformat) }}</span>
{%- endif -%}
</div>
{%- if desc -%}
<div class="desc">{{ desc }}</div>
{%- endif -%}
</div>
