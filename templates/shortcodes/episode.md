{%- set dateformat = "%e %b %Y" -%}
### S
{%- if season < 10 %}0{% endif %}{{ season }}
{%- if episode %}E{% if episode < 10 %}0{% endif %}{{ episode }}{% endif %}
{%- if episodes %}E{% for episode in episodes %}{% if episode < 10 %}0{% endif %}{{ episode }}{% if not loop.last %}-{% endif %}{% endfor %}{% endif %}
{%- if title %} {{ title }}{% endif %}
<div class="meta">
<div>
{%- if year -%}
<span class=tag>{{ year }}</span>
{%- endif -%}
{%- if link -%}
<span class=tag><a href="{{ link | safe }}">{{ link | split(pat="/") | nth(n=2) }}</a></span>
{%- endif -%}
{%- if links -%}
{%- for link in links -%}
<span class=tag><a href="{{ link | safe }}">{{ link | split(pat="/") | nth(n=2) }}</a></span>
{%- endfor -%}
{%- endif -%}
{%- if started and finished -%}
{%- set m1 = started | date(format="%m") | int -%}
{%- set m2 = finished | date(format="%m") | int -%}
{%- set y1 = started | date(format="%Y") | int -%}
{%- set y2 = finished | date(format="%Y") | int -%}
{%- if y1 == y2 -%}
    {%- if m1 == m2 -%}
        {%- set short = "%e" -%}
    {%- else -%}
        {%- set short = "%e %b" -%}
    {% endif %}
{%- else -%}
    {%- set short = dateformat -%}
{%- endif %}
<span class="sub">Watched from {{ started | date(format=short) }} to {{ finished | date(format=dateformat) }} </span>
{%- endif -%}
{%- if started and not finished -%}
<span class="sub">Watching since {{ started | date(format=dateformat) }}</span>
{%- endif -%}
{%- if seen -%}
<span class="sub">Watched {{ seen | date(format=dateformat) }}</span>
{%- endif -%}
</div>
</div>
