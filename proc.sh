#!/usr/bin/env bash

blog=content/posts

proc() {
    local slug="$1"
    local file="$2"

    if [ -f "${file}" ]; then
        perl -pi -e "s|\b---\b|—|g;" "${file}" # emdash
        perl -pi -e "s|\b--\b|–|g;" "${file}" # endash

        # quotation marks
        local meta="${slug}.meta"
        local cont="${slug}.cont"
        perl -0777p -e "s|(\+\+\+.*\+\+\+\n).*|\1|gs;" "${file}" > "${meta}"
        perl -0777p -e "s|\+\+\+.*\+\+\+\n(.*)|\1|gs;" "${file}" > "${cont}"
        perl -pi -e "s|([^=])\"([^\"]*?[^=])\"|\1“\2”|g;" "${cont}"
        perl -pi -e "s|([^=])'([^']*?[^=])'|\1‘\2’|g;" "${cont}"
        cat "${meta}" "${cont}" > "${file}"
        rm -f "${meta}" "${cont}"

        perl -pi -e "s|'|’|g;" "${file}" # apostrophe
    else
        echo "File $file doesn't exist!"
    fi
}

if [ "$1" ]; then
    file="$1"
    tmp="${file%.md}"
    slug="${tmp#${blog}/}"
    proc "${slug}" "${file}"
else
    for file in ${blog}/[^_]*.md; do
        filename=${file#${blog}/}
        slug=${filename%.md}
        proc "${slug}" "$file"
    done
fi
