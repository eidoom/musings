# [ˈmjuːzɪŋz](https://gitlab.com/eidoom/musings)

DEPRECATED [see new project](https://gitlab.com/eidoom/stories/)

* I renamed the website to ˈstɔːriz
* It's live [here](https://somestories.vercel.app/)

## Log
* Framework: [Zola](https://www.getzola.org/)
* Hosting: [Vercel](https://vercel.com/) (following [this](https://www.getzola.org/documentation/deployment/vercel/))
* Initial set up using [Zola's Getting Started](https://www.getzola.org/documentation/getting-started/overview/)
* initial [design](https://jgthms.com/web-design-in-4-minutes/)
* [colours](https://ethanschoonover.com/solarized/)
* search based on [Zola docs search](https://github.com/getzola/zola/blob/master/docs/static/search.js)
* font [Merriweather](https://github.com/SorkinType/Merriweather)

## Doc links
* [Framework Zola](https://www.getzola.org/documentation)
* [Templating engine Tera](https://tera.netlify.app/docs)

## Usage
* Build: `zola build`
* Development server: `zola serve`

## TODO
* ˈstɔːriz is rendered with default font for the non-ascii characters! This makes them not align with the other characters.
* Subtle animation for dropdown/search menus?
* Add tags to search
* Add dark theme toggle
* Use grid for archive
    * Done for write, but yet to do for edit and consume
        * How to `groupby` for these fields?
* in `content`, replace `'` with `’` and `"..."` with `“...”`
    * Can preprocess with `./proc.sh <file>`
* Instead of using `draft=false`, use `published=<date>`
    * Show +date_add >date_pub ^date_edit or similar?
* Mobile layout/top bar is broken
    * should redesign "mobile first"
* Pagination is not obvious enough
    * Show # posts not # pages?
    * Only show pagination tabs in sticky footer at bottom of page scroll?
    * Make footer fixed at page bottom (not sticky)?
    * Would be nice to have dynamic pagination, but then would have to completely redesign (not Zola)
        * ie paginate by different # for screen size
        * and next post relative to current group
* Should header be sticky?
* If styling gets good enough, package as Zola theme
* Organise collections (short stories, TV seasons) as subsections
    * Would require more flexible/custom static site engine

## Future development
* Branches `subsections` and `custom` are stale
* Looking into CSR version at <https://gitlab.com/eidoom/stories>

## Content notes
* I don’t really know anything about [IPA](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet); it was initially a unicode support test, then I just thought it looked cool.
* ~~A potentially controversial note on genre definition: *science fiction* is taken as a subset of *fantasy*.~~
Wikipedia convinced me that the term I am looking for is [speculative fiction](https://en.wikipedia.org/wiki/Speculative_fiction), which is a superset of both.
* Short story < novelette < novella < novel

## Legal

* This project is licensed under the GNU Affero General Public License v3.0.
* A copy of the licence can be found [here](LICENSE)
* More details are on the website [here](https://somestories.vercel.app/licence)
