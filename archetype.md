+++
title = "$title"
description = ""
date = $date
updated = $date
draft = false
[taxonomies]
media = ["$medium"]
formats = ["$format"]
tags = []
years = ["$year"]
artists = []
series = []
universe = []
[extra]
active = false
published = $date
consumed = []
#number = []
#chrono =
links = [$link]
+++

