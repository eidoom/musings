#!/usr/bin/env python3

import argparse
import pathlib
import datetime
import string

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create new entry")
    parser.add_argument(
        "directory",
        type=str,
        choices=["posts", "todo"],
        help="The target directory",
    )
    parser.add_argument(
        "title",
        type=str,
        help="The title of the story",
    )
    parser.add_argument(
        "kind",
        type=str,
        help="The medium or format of the story",
    )
    parser.add_argument(
        "year",
        type=int,
        help="The release year",
    )
    parser.add_argument(
        "-l",
        "--link",
        metavar="L",
        type=str,
        help="Wikipedia link",
    )
    args = parser.parse_args()

    slug = args.title.lower().replace(" ", "-").replace("–","-")

    file = pathlib.Path(f"content/{args.directory}/{slug}.md")

    if not file.exists():
        subs = {
            "title": args.title,
            "date": datetime.date.today().isoformat(),
            "year": args.year,
            "link": f'"{args.link}"' if args.link else "",
        }

        if args.kind in ("web", "game", "book", "video"):
            subs["medium"] = args.kind
            subs["format"] = ""
        else:
            subs["format"] = args.kind

            if args.kind in ("film", "television"):
                subs["medium"] = "video"
            elif args.kind in ("computer game", "board game", "card game", "video game"):
                subs["medium"] = "game"
            elif args.kind == "blog":
                subs["medium"] = "web"
            elif args.kind in ("novel", "short story", "novella", "comic"):
                subs["medium"] = "book"

        with open("archetype.md", "r") as f:
            template = f.read()

        s = string.Template(template)

        o = s.substitute(subs)

        with open(file, "w") as f:
            f.write(o)

    else:
        print(f"File {file} already exists!")
